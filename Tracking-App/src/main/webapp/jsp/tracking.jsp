<%@ page contentType="text/html; ISO-8859-1; charset=UTF-8" language="java" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="com.projet.hibernate.model.Tracking" %>
<%@ page import="com.projet.hibernate.model.Activity" %>
<%@ page import="com.projet.hibernate.model.Sport" %>
<%
     response.setHeader("Pragma", "no-cache");
     response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
     response.setHeader("Cache-Control", "pre-check=0, post-check=0");
%>

<html>
<head>
	<title>Tracking</title>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
	<script src="http://www.openstreetmap.org/openlayers/OpenStreetMap.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <link href="css/app.css" rel="stylesheet">
    <link href="css/trackings.css" rel="stylesheet">
	<script type="text/javascript" src="js/background.js"></script>
    <script type="text/javascript" src="js/statistics.js"></script>
    <% 
        String gpx_filename = (String)request.getAttribute("gpx_filename");
        Sport sport = (Sport)request.getAttribute("sport");
        Activity activity = (Activity)request.getAttribute("activity");
        List<Tracking> trackings = activity.getTrackings();
        %>

	<script type="text/javascript">
		var zoom=13

        var map;
        var layerMarkers;

		function init() {
            if (map == null) {
                map = new OpenLayers.Map ("map", {
				controls:[
					new OpenLayers.Control.Navigation(),
					new OpenLayers.Control.PanZoomBar(),
					new OpenLayers.Control.LayerSwitcher(),
					new OpenLayers.Control.Attribution()],
				maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
				maxResolution: 156543.0399,
				numZoomLevels: 19,
				units: 'm',
				projection: new OpenLayers.Projection("EPSG:900913"),
				displayProjection: new OpenLayers.Projection("EPSG:4326")
                } );

                layerMapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
                map.addLayer(layerMapnik);
                layerCycleMap = new OpenLayers.Layer.OSM.CycleMap("CycleMap");
                map.addLayer(layerCycleMap);
                layerMarkers = new OpenLayers.Layer.Markers("Markers");
                map.addLayer(layerMarkers);

                var lgpx = new OpenLayers.Layer.Vector("Descrizione del layer", {
                    strategies: [new OpenLayers.Strategy.Fixed()],
                    protocol: new OpenLayers.Protocol.HTTP({
                        url: '<%= gpx_filename %>',
                        format: new OpenLayers.Format.GPX()
                    }),
                    style: {strokeColor: "red", strokeWidth: 5, strokeOpacity: 0.5},
                    projection: new OpenLayers.Projection("EPSG:4326")
                });

                map.addLayer(lgpx);

                var lonLat = new OpenLayers.LonLat('<%= trackings.get(trackings.size() - 1).getLongitude() %>', '<%= trackings.get(trackings.size() - 1).getLatitude() %>').transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                map.setCenter(lonLat, zoom);

                var size = new OpenLayers.Size(21, 33);
                var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                var lastPoint = 'img/final_marker.webp';
                <%
                    if (activity.isProgress()) {
                        %>
                        lastPoint = 'img/final_marker_not_finished.webp';
                        <%
                    }
                %>
                var icon = new OpenLayers.Icon(lastPoint,size,offset);
                layerMarkers.addMarker(new OpenLayers.Marker(lonLat,icon));

                var startLonLat = new OpenLayers.LonLat('<%= activity.getLongitude() %>', '<%= activity.getLatitude() %>').transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                var firstPoint = 'img/start_marker.webp';
                
                var startPointIcon = new OpenLayers.Icon(firstPoint,size,offset);
                layerMarkers.addMarker(new OpenLayers.Marker(startLonLat,startPointIcon));
            }
        }

        function addMarker(latitude, longitude) {
            init();

			var size = new OpenLayers.Size(21, 33);
			var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
            var longitude_latitude = new OpenLayers.LonLat(longitude, latitude).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
            var icon = new OpenLayers.Icon('img/intermediaire_marker.webp',size,offset);
            layerMarkers.addMarker(new OpenLayers.Marker(longitude_latitude,icon));
        }
        </script>

    </head>
    <jsp:include page="component/navbar.jsp" />  


    <body onload="init();">
    <img src="img/home.png" class="background"/> 
        <div class="container-global">
            <div class="first-vertical div-flex">
                <div style="margin:auto;width:50%;display:flex;align-items:center;flex-direction:column;">
                    <h1> <span style="color: #e9ecef">Activité de : <%= sport.getName() %></span></h1>
                    <table id="table-data-activite" class="table table-striped table-dark table-bordered table-padding">
                    <tr>
                        <td>Début d'activité :</td>
                        <td><span style="color:#e9ecef"><%= activity.getStart_date().toString() %></span>
                    </tr>
                    <tr>
                        <td>Statut :</td>
                        <%
                        if (activity.isProgress()) {%>
                        <td><span style="color:red">En cours</span></td>
                        </tr>
                        <%}
                        else{ %>
                        <td><span style="color:#05F405">Fini</span></td>
                        </tr>
                        <tr>
                            <td>Fin d'activité :</td>
                            <td><span style="color:#e9ecef"><%= trackings.get(trackings.size() - 1).getDate().toString()%></span></td>
                        </tr>
                        <%
                        }%>
                    </table>
                </div>
            </div>
            <div class="second-vertical div-flex">
                <div style="border:1px solid white;height:100%;width:100%;" id="map">
                    <% 
                    for(int i = 0; i < trackings.size(); i++){
                        if (i != trackings.size() - 1) {
                    %>
                        <script>
                            document.addEventListener("DOMContentLoaded", () => {
                                addMarker('<%= trackings.get(i).getLatitude() %>', '<%= trackings.get(i).getLongitude() %>');
                            });
                        </script>
                    <%
                        }
                    }
                    %>
                </div>
            </div>
            <div class="third-vertical div-flex">
                <div class="container-flex-row">
                    <div class="first-row">
                    <table id="table-balise" class="table table-striped table-dark table-bordered table-padding">
                        <tr>
                            <td>Position de départ :</td>
                            <td><img src="img/start_marker.webp"/></td>
                        </tr>
                        <tr>
                            <td>Balise intermédiaire :</td>
                            <td><img src="img/intermediaire_marker.webp"/></td>
                        </tr>
                        <tr>
                            <td>Balise finale d'une activité non finie :</td>
                            <td><img src="img/final_marker_not_finished.webp"/></td>
                        </tr>
                        <tr>
                            <td>Balise finale d'une activité finie :</td>
                            <td><img src="img/final_marker.webp"/></td>
                        </tr>
                    </table>
                    </div>
                    <div class="second-row">
                        <table id="table-data-gps" class="table table-striped table-dark table-bordered">
                            <thead class="thead-dark">
                                <tr class="bg-info">
                                    <th scope="col" style="font-family: 'Roboto', sans-serif; font-size: 20px;">Etape</th>
                                    <th scope="col" style="font-family: 'Roboto', sans-serif; font-size: 20px;">Longitude</th>
                                    <th scope="col" style="font-family: 'Roboto', sans-serif; font-size: 20px;">Latitude</th>
                                    <th scope="col" style="font-family: 'Roboto', sans-serif; font-size: 20px;">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Départ</td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= activity.getLongitude() %></td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= activity.getLatitude() %></td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= activity.getStart_date().toString() %></td>
                                </tr>
                        <% 
                            for(int i = 0; i < trackings.size(); i++){
                            %>
                                <tr>
                                    <th scope="row"><%= (i + 1) %></td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= trackings.get(i).getLongitude() %></td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= trackings.get(i).getLatitude() %></td>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 20px;"><%= trackings.get(i).getDate().toString() %></td>
                                </tr>
                                <%
                            }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>