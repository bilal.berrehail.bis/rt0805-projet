package com.projet.utils.constante;

public interface ConstanteToken {
    final String error_token_not_in_request = "Un problème est survenu avec votre session. Reconnectez-vous svp";
    final String error_token_incorrect = "Un problème est survenu avec votre session. Reconnectez-vous svp";
    final String error_token_perime = "Votre session a expiré. Reconnectez-vous svp";
    final String error_user_token = "Aucun User existant associé à ce token";
 }