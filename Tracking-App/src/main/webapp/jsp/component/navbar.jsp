<%@ page contentType="text/html; charset=UTF-8" language="java"%> 

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/nav_bar.css" rel="stylesheet">
<link src="js/nav_bar.js" type="text/javascript">

    <div class="first header"> 
          <div class="nav_left">
              <div class="div_nav_petit">
                  <div id="div_home" class="div_nav">
                      <button id="btn_home" class="btn btn_nav">
                          <i id="i_home" class="fa i_btn"></i>
                      </button>
                      <span class="span_btn" id="home" bold><strong>Home</strong></span>
                  </div>
              </div>
              <div class="div_nav_grand">

              </div>
          </div>
          <div class="nav_right">
              <div class="div_nav_grand">

              </div>
              <div class="div_nav_petit">
                  <div style="margin-right: 10%"><strong></strong> </div>
                  <div id="div_disconnect" class="div_nav">
                      <button id="btn_disconnect" class="btn btn_nav">
                          <i id="i_disconnect" class="fa i_btn"></i>
                      </button>
                      <span class="span_btn" id="disconnect"><strong>Disconnect</strong></span>
                  </div>
              </div>
          </div>
    </div>
    <script> 
       let $disconnect_div = $('#div_disconnect');
        $disconnect_div.click( function(event){
            console.log("disconnect clicked");
            window.location.href = 'disconnect'
        });	
        let $back_to_home_div = $('#div_home');
        $back_to_home_div.click(function(event){
            console.log("home clicked");
            window.location.href = 'home'
        });
    </script>

     