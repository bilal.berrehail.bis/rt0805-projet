package com.projet.token;

public class TokenJwt {
    private int user_id;

    public TokenJwt(int user_id) {
        this.user_id = user_id;
    }

    public Integer getUser_id() {
        return user_id;
    }
}
