package com.projet.web;

import java.io.IOException;
import java.lang.reflect.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.projet.token.TokenJwt;
import com.projet.utils.RequestUtils;
import com.projet.utils.TokenUtils;
import com.projet.utils.constante.ConstanteToken;

import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Login", urlPatterns = "/login")
public class Login extends HttpServlet implements ConstanteToken {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // -- S'il est déjà connecté, on le redirige vers la page d'accueil --
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);
        if (tokenJwt != null) {
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/home");
            return;
        }

        // -- S'il vient de s'inscrire, on affiche un message et on pré remplie le champ email --
        HttpSession httpSession = request.getSession();
        if (httpSession.getAttribute("email_login_page") != null) {
            request.setAttribute("email_prefilled", httpSession.getAttribute("email_login_page"));
            httpSession.removeAttribute("email_login_page");
        }
        if (httpSession.getAttribute("message_login_page") != null) {
            request.setAttribute("success_message", httpSession.getAttribute("message_login_page"));
            httpSession.removeAttribute("message_login_page");
        }

        // -- End --
        request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // -- Ouverture simple de la page de login --
        if (request.getParameter("email") == null || request.getParameter("password") == null) {
            doGet(request, response);
            return;
        }

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // -- Requête HTTP vers l'API utilisateur --
        HashMap<String, String> data = new HashMap<>();
        data.put("email", email);
        data.put("password", password);

        String responseUserAPI = RequestUtils.requestPostAPI(request, "/api/user", data);

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        if (responseUserAPI.equals("")) {
            displayError("Impossible de questionner l'API", request, response);
            return;
        }

        Map<String, String> responseDecode = new Gson().fromJson(responseUserAPI, type);

        // -- Traitement de la réponse --
        // - code 0 : error
        // - code 1 : success
        if (responseDecode.get("code").equals("0")) {
            displayError(responseDecode.get("msg"), request, response);
        } else {
            String token = responseDecode.get("token");

            HttpSession httpSession = request.getSession();
            httpSession.setAttribute("token_api", token);

            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/home");
        }
    }

    private void displayError(String error_message, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("error_message", error_message);
        request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
    }
}