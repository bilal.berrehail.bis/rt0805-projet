SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l\'activité',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'FK : Identifiant de l\'utilisateur',
  `sport_id` int(10) UNSIGNED NOT NULL COMMENT 'FK : Identifiant du sport',
  `start_date` datetime(6) NOT NULL COMMENT 'Date de début de l\'activité',
  `latitude` double(10, 6) NOT NULL COMMENT 'Latitude du point de départ',
  `longitude` double(10, 6) NOT NULL COMMENT 'Longitude du point de départ',
  `is_archive` bit(1) NOT NULL COMMENT 'Archivé',
  `is_progress` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Activité en cours',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_user_id`(`user_id`) USING BTREE,
  INDEX `fk_sport_id`(`sport_id`) USING BTREE,
  CONSTRAINT `fk_sport_id` FOREIGN KEY (`sport_id`) REFERENCES `sport` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sport
-- ----------------------------
DROP TABLE IF EXISTS `sport`;
CREATE TABLE `sport`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifiant du sport',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Libellé du sport',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tracking
-- ----------------------------
DROP TABLE IF EXISTS `tracking`;
CREATE TABLE `tracking`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de tracking',
  `activity_id` int(10) UNSIGNED NOT NULL COMMENT 'FK : Identifiant de l\'activité',
  `latitude` double(10, 6) NOT NULL COMMENT 'Latitude de la localisation',
  `longitude` double(10, 6) NOT NULL COMMENT 'Longitude de la localisation',
  `date` datetime(6) NOT NULL COMMENT 'Date à laquelle le point a été parcouru',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_activity_id`(`activity_id`) USING BTREE,
  CONSTRAINT `fk_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l\'utilisateur',
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Prénom de l\'utilisateur',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Nom de l\'utilisateur',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Mot de passe de l\'utilisateur',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Email de l\'utilisateur',
  `age` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Âge de l\'utilisateur',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
