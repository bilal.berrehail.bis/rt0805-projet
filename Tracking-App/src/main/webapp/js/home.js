$( document ).ready(function() {
    $('.data-duration').each(function(){
       $(this).text(hour_from_double($(this).attr('duration')));
    });
/******************************************************************************************************************* 
 *                                      USER COMPONENT BIND CLICK ON BUTTTON modifier, valider, cancel
 ******************************************************************************************************************/
    
    $('#btn-modify-info').bind('click', function(event) {
        
        $('.inp_info_user ').removeAttr('disabled');
        
        let new_nodeBuilder = $("<div></div>")
            .attr("class","form-group temp_div");
        let new_inpBuilder = $("<input>")
            .attr('type','password')
            .attr('class','form-control inp_info_user');
        let new_inp_newpass = new_inpBuilder.clone()
            .attr('name','new_password')
            .attr('id','inp_new_password')
            .attr('placeholder','Nouveau mot de passe');
        let new_inp_newpassconfirmed = new_inpBuilder.clone()
            .attr('name','confirm_password')
            .attr('id','inp_confirm_password')
            .attr('placeholder','Confirmer votre nouveau mot de passe');
        let new_inp_pass = new_inpBuilder.clone()
            .attr('name','password')
            .attr('required','required')
            .attr('id','inp_password')
            .attr('placeholder','Mot de passe actuel');
        let div_btn_success =  $('#form-info .form-group').last();
        // On add le message de tips
        if($('#container_indication_user').length == 0 ){
            let new_node_indic = $("<div></div>")
                .attr("class","text-center")
                .attr("id","container_indication_user");
            let new_p = $("<p></p>")
                .text("Entrez votre mot de passe avant de confimer les changements")
                .attr("id","indication_user")
                .attr("class","p_tips")
            $(new_node_indic).append($(new_p));
            $(new_node_indic).insertBefore($(div_btn_success));
            div_btn_success = $('#container_indication_user');
        }
        else{
            $('#indication_user').text("Entrez votre mot de passe avant de confimer les changements").attr("class","p_tips");
            div_btn_success = $('#container_indication_user');
        }
        div_btn_success = $('#container_indication_user');
        $(new_nodeBuilder).clone().append($(new_inp_pass)).insertBefore($(div_btn_success));
        $(new_nodeBuilder).clone().append($(new_inp_newpass)).insertBefore($(div_btn_success));
        $(new_nodeBuilder).clone().append($(new_inp_newpassconfirmed)).insertBefore($(div_btn_success));
        
        let new_btn_valid = $('<button></button>')
            .attr("class","btn btn-success btn-block btn-valid-info")
            .attr("id", "btn-valid-info")
            .text("Confirmer");
        let new_btn_cancel = $('<button></button>')
            .attr("class","btn btn-danger btn-block btn-cancel-info")
            .attr("id", "btn-cancel-info")
            .text("Annuler");
        let div_row =$('<div></div>')
            .attr("class","row");
        let div_row_btnBuilder = $('<div></div>')
            .attr("class","col-xs-6");
        let div_row_btn_valid = $(div_row_btnBuilder).clone().append($(new_btn_valid));
        let div_row_btn_cancel = $(div_row_btnBuilder).clone().append($(new_btn_cancel));
        $(div_row).append($(div_row_btn_valid)).append(div_row_btn_cancel);
        $(new_nodeBuilder).clone().append($(div_row)).insertAfter($(div_btn_success));
        // On supprime le bouton 
        $(this).hide();
    });

    $(document).on("click", ".btn-valid-info", function(event) {   
        event.preventDefault();
        let parameter = '';
        if($('#inp_first_name').val() != ''){
            parameter+='first_name='+$('#inp_first_name').val()+'&';
        }
        if($('#inp_last_name').val() != ''){
            parameter+='last_name='+$('#inp_last_name').val()+'&';
        }
        if($('#inp_age').val() != ''){
            parameter+='age='+$('#inp_age').val()+'&';
        }
        if($('#inp_email').val() != ''){
            parameter+='email='+$('#inp_email').val()+'&';
        }
        if($('#inp_new_password').val() != ''){
            parameter+='new_password='+$('#inp_new_password').val()+'&';
        }
        if($('#inp_confirm_password').val() != ''){
            parameter+='confirm_password='+$('#inp_confirm_password').val()+'&';
        }
        if($('#inp_password').val() != ''){
            parameter+='password='+$('#inp_password').val()+'&';
            $.ajax({
                url: "api/user",
                type: 'PUT',
                data: parameter,
                 success: function(result){
                    if(result.code == "0"){
                        $("#indication_user")
                            .text(result.msg)
                            .attr("class","p_error")
                    }
                    else{
                        $("#indication_user")
                            .text(result.msg)
                            .attr("class","p_success")
                        // on disable les champs du formulaire
                        $('.inp_info_user ').attr('disabled','disabled');
                        // On supprime les div rajoutés précédemment
                        $('.temp_div').remove();
                        // On réaffiche le button initial
                        $('#btn-modify-info').show(); 
                    }
              }});
        }
        else{
            $('#indication_user').text("Entrez votre mot de passe avant de confimer les changements").attr("class","p_error");
            $('#inp_password').css("border-color","red");
        }
        
    });

    $(document).on("click",".btn-cancel-info", function(event) {
        event.preventDefault();
        if($('#indication_user')){
            $('#container_indication_user').remove();
        }
        // on disable les champs du formulaire
        $('.inp_info_user ').attr('disabled','disabled');
        // On supprime les div rajotués précédemment
        $('.temp_div').remove();
        // On supprime les boutons valider et annuler
        $(this).remove();
        $('#btn-valid-info').remove();
        // On réaffiche le button initial
        $('#btn-modify-info').show();
    });

    
    
/*** ICON ACTION ***/
$(".icon-action-more").click(function() {
    window.location = $(this).data("href");
});

$('.icon-action-delete').click(function(event){
    var windows_confirm = confirm("Voulez vous vraiment archiver cette activité ? Cette action est irrerversible. Les activités archivés ne sont plus comptés dans les statistiques des sports. Vous pourrez tout de même la consulter en l'affichant à l'aide des filtres.");
    let $parent  = $(event.target).parent('.activity-action')
    let sport_id = $($parent).siblings('.activity-sport').attr('sport-id');
    let activity_duration = $($parent).siblings('.activity-duration').attr('duration');
    
    
    if( windows_confirm == true ) {
        let activity_id = $(event.target).parent('.activity-action').siblings('.activity-id').text();
        let parameter = 'actv_id='+activity_id+'&test=test&tast=tast';
        $.ajax({
            url: "api/activity?actv_id=" + activity_id,
            type: 'DELETE',
            success: function(result){
            if(result.activity_id != -1){
                $current_line = $(event.target).parent('.activity-action').parent('.activity-no-archived');
                $($current_line).removeClass('.activity-no-archived').addClass('.activity-archived');
                $(event.target).remove();
                let input_activity_type = $('#activity-type').val();
                if(input_activity_type != 'all' && input_activity_type != 'archive'){
                    $($current_line).hide();
                }
                // On met à jour les data des sports
                let $data_sports = $('.line-data-sports');
                $data_sports.each(function(e){
                    let sport_sport_id = $(this).find('.sport-id').text();
                    if(sport_sport_id == sport_id){
                        let $sport_nb_act = $(this).find('.sport-nb-act');
                        let nb_activity = parseInt($($sport_nb_act).text(),10);
                        if(nb_activity == 1 ){
                            $(this).remove();
                        }
                        else{
                            $($sport_nb_act).text((nb_activity-1).toString());
                            let $sport_duration = $(this).find('.sport-duration');
                            $($sport_duration).attr('duration',$($sport_duration).attr('duration') - activity_duration);
                            $($sport_duration).text(hour_from_double($($sport_duration).attr('duration')));
                        }
                    }
                })
            }
            else{
                alert(result.msg); 
            }
          }});
    }
})

/******************************************************************************************************************* 
 *                                         SPORT COMPONENT : CLICK FILTER AND CREATE INPUT FILTERS
 ******************************************************************************************************************/

    $('#btn-sport-filter').bind('click', function(event) {
        
        event.preventDefault();
        let $div_temp_sport = $('.div-filter-sport');
        let $input_hidden_sport = $('#is-div-sport');
        if($($input_hidden_sport).length == 0 ){
            $input_hidden = $('<input>').attr('id','is-div-sport').hide();
            $('#container-list-sport').append($input_hidden);
            create_input_sport_filter();
            generic_create_btn_filter('sport');
            generic_p_filter('sport');
        }
        else{
            $($div_temp_sport).show();
        }
        $('.div-data-sport').hide();
    });

    
function create_input_sport_filter(){
    let $div_input_sport = $('.div-filter-input-sport');
    if($($div_input_sport).length == 0){
        let now = Date.now();
        let $div_sport_no_archived = $('#container-list-sport');
        let $new_nodeBuilder = $('<div></div>')
            .attr('class','form-group div-filter-sport div-filter-input-sport');
        let $div_rowBuilder =$('<div></div>')
            .attr('class','row row-input-form');
        let $div_colBuilder = $('<div></div>')
            .attr('class','col-xs-6');
        let $labelBuilder = $('<label></label>')
            .attr('class','input-group-text label-sport')
        let $inputBuilder = $('<input>')
            .attr('type','password')
            .attr('class','form-control inp-sport-filter filter-sport');
        let $selectBuilder = $('<select></select>')
            .attr('class','custom-select form-control select-sport-filter filter-sport');
        let $optionBuilder = $('<option></option>')
            .attr('class','option-sport-sport-name');
        $data_ref = $('#ref_sport');
        let attr_duration_atleast = {"max":delete_comma($($data_ref).attr("max_duration")), "min":delete_comma($($data_ref).attr("min_duration")), 'defaut':delete_comma($($data_ref).attr("min_duration"))};
        let attr_duration_atmost = {"max":delete_comma($($data_ref).attr("max_duration")), "min":delete_comma($($data_ref).attr("min_duration")), 'defaut':delete_comma($($data_ref).attr("max_duration"))};
       
        let attr_nb_activities_atleast = {"max":$($data_ref).attr("max_nb_activity"),"min":$($data_ref).attr("min_nb_activity"), defaut:$($data_ref).attr("min_nb_activity")};
        let attr_nb_activities_atmost = {"max":$($data_ref).attr("max_nb_activity"),"min":$($data_ref).attr("min_nb_activity"), defaut:$($data_ref).attr("max_nb_activity")};
        
        // Durée au moins, au plus
        let $row_duration = $($div_rowBuilder).clone()
            .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'duration-at-least','number', 'Durée au moins :', attr_duration_atleast['min'], attr_duration_atleast))
            .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'duration-at-most','number', 'Durée au plus :', attr_duration_atmost['max'], attr_duration_atmost));
        // Nombre d'activités au moins, au plus
        let $row_number_activity = $($div_rowBuilder).clone()
            .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'number-activity-at-least','number', 'Nombre d\'activités au moins :', attr_nb_activities_atleast['min'], attr_nb_activities_atleast,[]))
            .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'number-activity-at-most','number',  'Nombre d\'activités au plus : ', attr_nb_activities_atmost['max'], attr_nb_activities_atmost,[]));
        // Sports practiced
        let tab_option_sports = generate_option_sport_name($optionBuilder,{'value':'all','text':'Tous','defaut':true});
        let $row_sport_practiced = $($div_rowBuilder).clone()
            .append(colSelectFactory($div_colBuilder,$labelBuilder,$selectBuilder,'sport-practiced','Sport pratiqués : (Select multiple : Ctrl)',{'multiple':true},tab_option_sports));
            
        let $node_date_filter = $($new_nodeBuilder).clone().append($($row_duration));
        let $node_number_activity = $($new_nodeBuilder).clone().append($($row_number_activity));
        let $node_sport_practiced = $($new_nodeBuilder).clone().append($($row_sport_practiced));
        
        $($node_sport_practiced).insertAfter($($div_sport_no_archived));
        $($node_date_filter).insertAfter($($div_sport_no_archived));
        $($node_number_activity).insertAfter($($div_sport_no_archived));
    }
    else{
        $($div_input_sport.show());
    }
}
/******************************************************************************************************************* 
 *                                        ACTIVITY COMPONENT : CLICK FILTER ET CREATE INPUT FILTERS
 ******************************************************************************************************************/
    $('#btn-activity-filter').bind('click', function(event) {
        event.preventDefault();
        let $div_temp_activity = $('.div-filter-activity');
        let $input_hidden_activity = $('#is-div-activity');
        if($($input_hidden_activity).length == 0 ){
            $input_hidden = $('<input>').attr('id','is-div-activity').hide();
            $('#container-list-activity').append($input_hidden);
            create_input_activity_filter();
            generic_create_btn_filter('activity');
            generic_p_filter('activity');
        }
        else{
            $($div_temp_activity).show();
        }
        $('.div-data-activity').hide();
    });

    function create_input_activity_filter(){
        let $div_input_activity = $('.div-filter-input-activity');
        if($($div_input_activity).length == 0){
            let now = new Date(Date.now());
            let $div_activity_no_archived = $('#container-list-activity');
            let $new_nodeBuilder = $('<div></div>')
                .attr('class','form-group div-filter-activity div-filter-input-activity');
            let $div_rowBuilder =$('<div></div>')
                .attr('class','row row_input_form');
            let $div_colBuilder = $('<div></div>')
                .attr('class','col-xs-6');
            let $labelBuilder = $('<label></label>')
                .attr('class','input-group-text label-activity')
            let $inputBuilder = $('<input>')
                .attr('type','password')
                .attr('class','form-control inp-activity_filter filter-activity');
            let $selectBuilder = $('<select></select>')
                .attr('class','custom-select form-control select-activity-filter filter-activity');
            let $optionSportBuilder = $('<option></option>') 
                .attr('class','option-activity-sport_name');
            let $optionTypeActBuilder = $('<option></option>')
                .attr('class','option-activity-activity_type');
            let $optionCurrentActBuilder = $('<option></option>')
                .attr('class','option-current-act');
            $data_ref = $('#ref_activity');
            let attr_duration_atleast = {"max":$($data_ref).attr("max_duration"), "min":$($data_ref).attr("min_duration"), 'defaut':$($data_ref).attr("min_duration")};
            let attr_duration_atmost = {"max":$($data_ref).attr("max_duration"), "min":$($data_ref).attr("min_duration"), 'defaut':$($data_ref).attr("max_duration")};
            
            for(key in attr_duration_atleast){
                
            }
            
            for(key in attr_duration_atmost){
                
            }
            let date_min = $($data_ref).attr("date_min");
            // Date avant apres
            let $row_date = $($div_rowBuilder).clone()
                .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'date-before-activity','text', 'Date avant :', "",{'class':'datepicker-input datepicker-input-max datepicker-activity'}))
                .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'date-after-activity','text', 'Date après :', "",{'class':'datepicker-input datepicker-input-min datepicker-activity'}));
            // Durée au moins, au plus
            let $row_duration = $($div_rowBuilder).clone()
                .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'duration-at-least','number', 'Durée au moins :', attr_duration_atleast['min'], attr_duration_atleast))
                .append(colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder,'duration-at-most','number', 'Durée au plus :', attr_duration_atmost['max'], attr_duration_atmost));
            // Sport pratiqués et type d'archive
            let tab_option_type = generate_option_type_activity($optionTypeActBuilder);
            let tab_option_sports = generate_option_sport_name($optionSportBuilder,{'value':'all','text':'Tous','defaut':true});
            let tab_option_act = [$($optionCurrentActBuilder).clone().text('Oui').attr('value','yes').attr("selected",true).attr("defaut",true),$($optionCurrentActBuilder).clone().text("Non").attr('value','no')];
            $colselecttypeact = colSelectFactory($div_colBuilder,$labelBuilder,$selectBuilder,'activity-type','Types d\'activités:',[],tab_option_type);
                $label_act_current = labelFactory($labelBuilder,'select-current-act','Afficher l\'activité en cours');
                $select_act_current = selectFactory($selectBuilder,'select-current-act',[],tab_option_act);
            $($colselecttypeact).append($label_act_current,$select_act_current);
            let $row_sport_archive = $($div_rowBuilder).clone()
                .append(colSelectFactory($div_colBuilder,$labelBuilder,$selectBuilder,'sport-practiced','Sport pratiqués : (Select multiple : Ctrl)',{'multiple':true},tab_option_sports))
                .append($($colselecttypeact));
            let $node_date_filter = $($new_nodeBuilder).clone()
                .append($($row_date))
                .append($($row_duration))
                .append($($row_sport_archive));
            $($node_date_filter).insertAfter($($div_activity_no_archived));
            $('.datepicker-input').datepicker({
                "format": "dd/mm/yy"
            });
            $('.datepicker-input-max').datepicker('setValue',now);
            $('.datepicker-input-min').datepicker('setValue',date_min);
            $('.datepicker-input').datepicker()
            .on('changeDate', function(event){
                let date_format_en = format_date(event.date,'en','/');
                $(event.target).attr('value',date_format_en);
            });
        }
        else{
            $($div_input_activity.show());
        }
    }

 /******************************************************************************************************************* 
 *                                       BIND CLICK ON BUTTON FILTER : CANCEL, VALID, RESET
 ******************************************************************************************************************/
    $(document).on('click','.btn-filter-cancel', function(event) {
        event.preventDefault();
        let entity = entity_of_component($(this));
        generic_btn_clicked(entity,false);
    });

     $(document).on('click','.btn-filter-valid', function(event) {
        event.preventDefault();
        let entity = entity_of_component($(this));
        generic_btn_clicked(entity,true);
        generic_confirm_filter(entity);
    });

    $(document).on('click', '.btn-reset-filter', function(event) {
        event.preventDefault();
        let entity = entity_of_component($(this));
        generic_reset_filter(entity);
    })
    
});


/******************************************************************************************************************* 
 *                                                  FACTORY COMPONENTS
 ******************************************************************************************************************/

function colInputFactory($div_colBuilder,$labelBuilder,$inputBuilder, id,type,texte,defaut='',attr=[],input_component = []){
    $new_col = colFactory($div_colBuilder,[]);
    $new_label = labelFactory($labelBuilder,id,texte,[]);
    $new_input = inputFactory($inputBuilder,id,type,texte,defaut,attr,input_component);
    //
    $new_col.append($new_label).append($new_input);
    return $new_col;
}

function colSelectFactory($div_colBuilder,$labelBuilder,$selectBuilder, id,texte,attr=[],options_component = []){
    $new_col = colFactory($div_colBuilder,[]);
    $new_label = labelFactory($labelBuilder,id,texte,[]);
    $new_select = selectFactory($selectBuilder, id, attr,options_component);
    $new_col.append($new_label).append($new_select);
    return $new_col;
}

function colFactory($div_colBuilder,attr=[]){
    $new_col = $($div_colBuilder).clone();
    for(key in attr){
        $new_col.attr(key,attr[key]);
    }
    return $new_col;
}

function labelFactory($labelBuilder,id,texte,attr = []){
    $new_label = $($labelBuilder).clone().attr('for',id).text(texte);
    for(key in attr){
        $new_label.attr(key,attr[key]);
    }
    return $new_label;
}

function inputFactory($inputBuilder,id,type,texte,defaut='',attr=[],input_component=[]){
    $new_input = $($inputBuilder).clone().attr('id',id).attr('type',type);
    if(isset(defaut)){
        $($new_input).attr('value',defaut);
    }
    for(key in attr){
        $new_input.attr(key,attr[key]);
    }
    if(isset(input_component,true)){
        input_component.forEach(value => $new_input.append(value));
    }
    return $new_input;
}

function selectFactory($selectBuilder, id, attr=[],options_component = []){
    $new_select = $($selectBuilder).clone().attr('id',id);
    for(key in attr){
        $new_select.attr(key,attr[key]);
    }
    if(isset(options_component,true)){
        options_component.forEach(value => $new_select.append(value));
    }
    return $new_select;
}

function hintTextFactory(text,attr=[]){
    $new_p = $('<p></p>').attr('class','hint-text ').text(text);
    if(isset(attr)){
        for(key in attr){
            if(key=='class'){
                $($new_p).attr(key,$($new_p).attr('class')+' '+attr[key]);
            }
            else{
                $($new_p).attr(key,attr[key]);
            }
        }
    }
    return $new_p;
}

function generate_option_sport_name($optionBuilder,defaut=[]){
    let tab_options = [];
    if(defaut.length != 0 ){
        $new_optionDefaut = $optionBuilder.clone().attr('value',defaut['value']).text(defaut['text']).attr('selected',true);
        tab_options.push($new_optionDefaut);
    }
    let tabd_sport = {};
    $('.sport-name').each( function(){
        let id = $(this).siblings('th').text();
        let name = $(this).text();
        tabd_sport[id]=name;
    });
    Object.keys(tabd_sport).map(
        function (k) {
            $new_option = $optionBuilder.clone().attr('value',k).text(tabd_sport[k]);
            tab_options.push($new_option);
        });
    return tab_options;
}

function generate_option_type_activity($optionBuilder) {
    let tab_options = [];
    tab_options.push($optionBuilder.clone().attr('value','no_archive').text('Non archivés').attr('selected',true).attr('defaut',true));
    tab_options.push($optionBuilder.clone().attr('value','archive').text('Archivés'));
    tab_options.push($optionBuilder.clone().attr('value','all').text('Tous'));
    return tab_options;
}

/******************************************************************************************************************* 
 *                                                  GENERIC FUNCTION : SPORT OR ACTIVITY
 ******************************************************************************************************************/
function generic_create_btn_filter(entity){
    let div_btn = $('#div-btn-'+entity);
    let new_nodeBuilder = $('<div></div>')
        .attr('class','form-group div-filter-'+entity+' div-filter-btn')
        .attr('id','div-filter-btn-'+entity);
    let new_btn_valid = $('<button></button>')
        .attr('class','btn btn-success btn-block btn-filter-valid')
        .attr('id', 'btn-filter-valid-'+entity)
        .text('Confirmer');
    let new_btn_cancel = $('<button></button>')
        .attr('class','btn btn-danger btn-block btn-filter-cancel')
        .attr('id', 'btn-filter-cancel-'+entity)
        .text('Annuler');
    let div_row =$('<div></div>')
        .attr('class','row');
    let div_row_btnBuilder = $('<div></div>')
        .attr('class','col-xs-6');
    let div_row_btn_valid = $(div_row_btnBuilder).clone().append($(new_btn_valid));
    let div_row_btn_cancel = $(div_row_btnBuilder).clone().append($(new_btn_cancel));
    $(div_row)
        .append($(div_row_btn_valid))
        .append(div_row_btn_cancel);
    $(new_nodeBuilder).clone().append($(div_row)).insertAfter($(div_btn));
}

function generic_btn_clicked(entity,confirm=false){
    // Si on a confirme des filtres on affiche le bouton pour les réinitialiser
    if(confirm==true){
        $div_reset_filter = $('#btn-reset-filter-'+entity);
        if($($div_reset_filter).length == 0 ){
            
            $div_btn = $('#div-btn-'+entity);
            
            let $new_btn = $('<button></button>')
                .attr('class','btn btn-light btn-block btn-reset-filter')
                .attr('id', 'btn-reset-filter-'+entity)
                .text('Réinitialiser les filtres');
            
            
            $($new_btn).insertAfter($($div_btn).find('.btn-primary'));
        }
    }
    $('.div-filter-'+entity).hide();
    $('.div-data-'+entity).show();
    $('#div-btn-'+entity).show();  
}

function generic_confirm_filter(entity){
    tab_input_value = {};
    $('.filter-'+entity).each(function(){
        tab_input_value[$(this).attr("id")] = $(this).val();
    });
    switch(entity){
        case'sport':
            filter_sport(tab_input_value);
        break;
        case'activity':
            $('.datepicker-activity').each(function(){
                tab_input_value[$(this).attr("id")] = $(this).attr('value');
            });
            filter_activity(tab_input_value);
        break;
        defaut:
            
            break;
    }
    
}

function generic_reset_filter(entity){
    
    $('.inp-'+entity+'-filter').each(function(){
        $(this).val($(this).attr('defaut'));
    });
    $('.select-'+entity+'-filter').each(function(){
        
        $(this).prop('selectedIndex',0);
    });
    $('#btn-reset-filter-'+entity).remove();
    switch(entity){
        case'sport':
            filter_sport([]);
        case'activity':
            filter_activity([]);
    }
}

 function generic_p_filter(entity){
    $new_text = hintTextFactory('Choisissez un ou plusieurs filtres',{'class':'div-filter-'+entity});
    $($new_text).insertAfter('#title-form-'+entity);
}




/******************************************************************************************************************* 
 *                                                 FILTERS FUNCTIONS
 ******************************************************************************************************************/
function filter_sport(array_filters){
    $data_ref = $('#ref_sport');
    let tab_duration = {"max":$($data_ref).attr("max_duration"), "min":$($data_ref).attr("min_duration")};
    let tab_nb_activities = {"max":$($data_ref).attr("max_nb_activity"),"min":$($data_ref).attr("min_nb_activity")};
    let nb_activities_min = isset(array_filters['number-activity-at-least']) ? array_filters['number-activity-at-least'] : tab_nb_activities['min'];
    let nb_activities_max = isset(array_filters['number-activity-at-most']) ? array_filters['number-activity-at-most'] : tab_nb_activities['max'];
    let duration_min = delete_comma(isset(array_filters['duration-at-least']) ? array_filters['duration-at-least'] : tab_duration['min']);
    let duration_max = delete_comma(isset(array_filters['duration-at-most']) ? array_filters['duration-at-most'] : tab_duration['max']);
    let sport_chosen = isset(array_filters['sport-practiced']) ? array_filters['sport-practiced'] : 'all';

    var $filterableRows = $('#table-list-sport').find('tr').not(':first');
    
    $($filterableRows).each(function(e){
        
    })
    $($filterableRows).hide();
        $filterableRows
            .filter(function() {    
                let nb_activity = parseInt($(this).find('.sport-nb-act').text());
                let duration = parseInt($(this).find('.sport-duration').text());
                let sport = $(this).find('.sport-id').text();
                let result = nb_activity >= nb_activities_min && nb_activity <= nb_activities_max && duration >= duration_min && duration <= duration_max;
                if(sport_chosen != 'all'){
                    if(!(sport_chosen.includes('all'))){
                        sport_chosen.forEach(function(value){
                            
                        });
                        if(!(sport_chosen.includes(sport))){
                            result = false;
                        }
                    }
                }
                return result;
            })
            .show();
}

function filter_activity(array_filters){
    $data_ref = $('#ref_activity');
    let tab_duration_ref = {'max':$($data_ref).attr('max_duration'), 'min':$($data_ref).attr('min_duration')};
    let date_min_ref =   $($data_ref).attr('date_min');
    let date_max_ref = Date.now();
    date_max_ref = new Date(date_max_ref);
    date_min_ref = new Date(date_min_ref);

    let date_min = isset(array_filters['date-after-activity']) ? new Date(array_filters['date-after-activity']) : date_min_ref;
    let date_max = isset(array_filters['date-before-activity']) ? new Date(array_filters['date-before-activity']) : date_max_ref;
    let duration_min = delete_comma(isset(array_filters['duration-at-least']) ? array_filters['duration-at-least'] : tab_duration_ref['min']);
    let duration_max = delete_comma(isset(array_filters['duration-at-most']) ? array_filters['duration-at-most'] : tab_duration_ref['max']);
    let activity_type = isset(array_filters['activity-type']) ? array_filters['activity-type'] : 'no_archive';
    let current_act = isset(array_filters['select-current-act']) ? array_filters['select-current-act'] : 'yes';
    let sport_chosen = isset(array_filters['sport-practiced']) ? array_filters['sport-practiced'] : 'all'; 
    
    current_act == 'no' ?  $('.data-current-activity').hide() : $('.data-current-activity').show();

    $('#table-activity').find('tr').not(':first').hide();
    if(activity_type == 'all'){
        var $filterableRows = $('#table-activity').find('tr').not(':first');
    }
    if(activity_type == 'archive'){
        var $filterableRows = $('#table-activity').find('.activity-archived');
    }
    if(activity_type == 'no_archive'){
        var $filterableRows = $('#table-activity').find('.activity-no-archived');
    }
    
    $($filterableRows).each(function(e){
        
    })
        $filterableRows
            .filter(function() {    
                let duration = parseInt($(this).find('.activity-duration').text());
                let sport = $(this).find('.activity-sport').attr('sport-id');
                let date = new Date($(this).find('.activity-date').attr('data-date'));
                let date_time = date.getTime();
                let result = duration >= duration_min && duration <= duration_max && date_time >= date_min && date_time <= date_max;
                if(sport_chosen != 'all'){
                    if(!(sport_chosen.includes('all'))){
                        sport_chosen.forEach(function(value){
                            
                        });
                        if(!(sport_chosen.includes(sport))){
                            result = false;
                        }
                    }
                }
                else{
                    
                }
                return result;
            })
            .show();
}



/*******************************************************************************************************************
 *                                                  UTILS
 *******************************************************************************************************************/
function date_to_input_value(date){
    return date.toISOString().split('T')[0];
}

function format_date(date,format,delimiter){
    let day = date.getDate().toString(10);
    let month = (date.getMonth()+1).toString(10);
    let year = date.getFullYear().toString(10);
    if(format=='fr'){
        return day+delimiter+month+delimiter+year;
    }
    if(format=='en'){
        return month+delimiter+day+delimiter+year;
    }
}

function isset(variable,is_array = false){
    if(is_array){
        return variable!=null&&variable.length>0&&variable!=='undefined';
    }
    return variable!=null&&variable!=''&&variable!=='undefined';
}

function delete_comma(time){
    let decimal_part_hour = time % 1 ; 
    let result = time - decimal_part_hour;
    return result;
}

function hour_from_double(time){
    let decimal_part_hour = time % 1 ; 
    let hour = time - decimal_part_hour;
    let minute = decimal_part_hour * 60 ;
    let decimal_part_minute = minute % 1 ; 
    minute = minute - decimal_part_minute;
    if(minute==0){
        minute="00"
    }
    else{
        minute = minute.toString(10)
    }
    result = hour.toString(10)+" h "+minute;
    return result;
}

function date_from_fr_date(date){
    let tab_date = date.split('/');
    let new_date = new Date(tab_date[tab_date[1],tab_date[0],tab_date[2]]);
    return new_date;
}

function entity_of_component($component){
    let id = $($component).attr('id');
    id = id.split('-');
    let entity = id[(id.length)-1];
    
    return entity;
}
