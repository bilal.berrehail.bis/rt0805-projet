package com.projet.api;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Tracking;
import com.projet.token.TokenJwt;
import com.projet.utils.TokenUtils;

@WebServlet(name = "TrackingAPI", urlPatterns = "/api/tracking")
public class TrackingAPI extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public class ResponseTrackingAPI {
        String message;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        response.setContentType("application/json; charset=UTF-8");

        ResponseTrackingAPI responseTrackingAPI = new ResponseTrackingAPI();
        
        if (tokenJwt == null) {
            responseTrackingAPI.message = "token incorrect";
            response.getWriter().println(new Gson().toJson(responseTrackingAPI));
            return;
        }

        String actv_idStr = request.getParameter("actv_id");
        String latitudeStr = request.getParameter("latitude");
        String longitudeStr = request.getParameter("longitude");

        if (actv_idStr == null || latitudeStr == null | longitudeStr == null) {
            responseTrackingAPI.message = "Paramètres incorrects";
            response.getWriter().println(new Gson().toJson(responseTrackingAPI));
            return;
        }

        Integer actv_id = Integer.valueOf(actv_idStr);

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("id", actv_id);
        conditions.put("user_id", tokenJwt.getUser_id());

        List<Activity> activity = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);

        if (activity == null) {
            return;
        }

        if (request.getParameter("end") != null) {
            activity.get(0).setProgress(false);
            hc.beginAndCommit();
        }

        Tracking tracking = new Tracking();
        tracking.setActivity_id(actv_id);
        tracking.setLatitude(Double.parseDouble(latitudeStr));
        tracking.setLongitude(Double.parseDouble(longitudeStr));
        tracking.setDate(new Date());
        hc.addEntity(tracking);
        responseTrackingAPI.message = "Ajout du point GPS réussie";
        response.getWriter().println(new Gson().toJson(responseTrackingAPI));
    }
}