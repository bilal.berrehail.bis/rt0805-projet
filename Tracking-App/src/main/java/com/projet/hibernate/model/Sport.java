package com.projet.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity(name = "sport")
@Table(name = "sport")
public class Sport implements HibernateModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, unique = true)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Transient
	private Integer nbActivites;
	
	public Sport() {
		
	}
	
	public Sport(String name) {
		this.name = name;
	}

	@Override
    public boolean equals(Object obj) { 
		if(obj == null || obj.getClass()!= this.getClass()) {
			return false;
		}
		Sport sport = (Sport) obj;
		return (sport.getName().equals(this.getName()) && sport.getId() == this.getId()); 
	}

	@Override
	public String toString(){
		return "id : "+this.getId()+" | name : "+this.getName();
	}
	
	public static String getEntityName() {
		return "sport";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNbActivites() {
		return nbActivites;
	}

	public void setNbActivites(Integer nbActivites) {
		this.nbActivites = nbActivites;
	}

	
}
