package com.projet.hibernate.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity(name = "activity")
@Table(name = "activity")
public class Activity implements HibernateModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, unique = true)
	private int id;
	
	@Column(name = "user_id", updatable = false)
	private int user_id;
	
	@Column(name = "sport_id", updatable = false)
	private int sport_id;
	
	@Column(name = "start_date")
	private Date start_date;

	@Column(name = "latitude")
	private double latitude;

	@Column(name = "longitude")
	private double longitude;

	@Column(name = "is_archive")
	private boolean is_archive;

	@Column(name = "is_progress")
	private boolean is_progress;
	
	@Transient
	private List<Tracking> trackings;
	
	public Activity() {
		
	}
	
	public static String getEntityName() {
		return "activity";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getSport_id() {
		return sport_id;
	}

	public void setSport_id(int sport_id) {
		this.sport_id = sport_id;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isArchive() {
		return is_archive;
	}

	public void setArchive(boolean is_archive) {
		this.is_archive = is_archive;
	}

	public boolean isProgress() {
		return is_progress;
	}

	public void setProgress(boolean is_progress) {
		this.is_progress = is_progress;
	}

	public List<Tracking> getTrackings() {
		return trackings;
	}

	public void setTrackings(List<Tracking> trackings) {
		this.trackings = trackings;
	}
}
