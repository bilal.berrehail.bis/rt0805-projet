package com.projet.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Sport;
import com.projet.hibernate.model.Tracking;
import com.projet.hibernate.model.User;
import com.projet.hibernate.query.HcQuery;
import com.projet.token.TokenJwt;
import com.projet.utils.RequestUtils;

import com.projet.utils.StatisticUtils;
import com.projet.utils.TokenUtils;
import com.projet.utils.constante.ConstanteToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.List;

@WebServlet(name = "Home", urlPatterns = "/home")
public class Home extends HttpServlet implements ConstanteToken {

    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        // -- Vérification --
        if (tokenJwt == null) {
            RequestUtils.clearRequest(request);
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
            return;
        }

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();

        // -- Récuperation de l'utilisateur --
        conditions.put("id", tokenJwt.getUser_id());
        List<User> list_user = hc.getEntities(User.class, User.getEntityName(), conditions);
        if (list_user == null || list_user.size() == 0) {
            RequestUtils.clearRequest(request);
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
            return;
        }
        User user = list_user.get(0);
        list_user.clear();
        
        // -- Récuperation des sports --
        conditions.clear();
        List<Sport> list_sport = hc.getEntities(Sport.class, Sport.getEntityName(), conditions);
        HashMap<Integer, Sport> map_sport = new HashMap<>();
        for (Sport sport : list_sport) {
            map_sport.put(sport.getId(), sport);
        }
        list_sport.clear();

        // -- Récupération de toutes les activités finis--
        conditions.clear();
        conditions.put("user_id", user.getId());
        conditions.put("is_progress",false);
        List<Activity> list_activity = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);
        conditions.clear();

        // Récupération de l'activité en cours
        conditions.put("user_id",user.getId());
        conditions.put("is_progress",true);
        List<Activity> list_activity_current = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);
        Activity current_activity = null;
        if(list_activity_current != null && list_activity_current.size() > 0){
            current_activity = list_activity_current.get(0);
        }
        list_activity_current.clear();
        conditions.clear();
        // Data qu'on stockera sur le client en javascript. Elles nous serviront pour les valeurs par défaut des filtres. 
        Double max_duration_activity = Double.MIN_VALUE,min_duration_activity = Double.MAX_VALUE,max_duration_sport = Double.MIN_VALUE,min_duration_sport = Double.MIN_VALUE;
        int max_nb_activity_sport = Integer.MIN_VALUE,min_nb_activity_sport  = Integer.MAX_VALUE;
        Date date_min_activity = new Date();
         
        // On récupère les trackings de chaque activité, et on calcule pour cque activité sa duration
        // On récupère aussi les sports pratiqués, et leur activité associé
        HashMap<Integer,List<Integer>> map_SportId_ActivitiesId = new HashMap<>();
        HashMap<Integer, Double> map_ActivityId_duration = new HashMap<>();
        for (Activity activity : list_activity) {
            // On calcule les durations
            Date date_start_activity = activity.getStart_date();
            date_min_activity = date_start_activity.compareTo(date_min_activity) < 0  ? date_start_activity : date_min_activity;  
            conditions.put("activity_id", activity.getId());
            List<Tracking> trackings = hc.getEntities(Tracking.class, Tracking.getEntityName(), conditions);
            activity.setTrackings(trackings);
            Double duration_activity = StatisticUtils.getDurationOfActivity(activity);
            max_duration_activity = duration_activity > max_duration_activity ? duration_activity : max_duration_activity;
            min_duration_activity = duration_activity < min_duration_activity ? duration_activity : min_duration_activity;
            map_ActivityId_duration.put(activity.getId(), duration_activity);
            // On lie chaque activité au sport pratiqué
            if(!activity.isArchive()){
                if(map_SportId_ActivitiesId.containsKey(activity.getSport_id())){
                    map_SportId_ActivitiesId.get(activity.getSport_id()).add(activity.getId());
                }
                else{
                    List<Integer> list_temp = new ArrayList<>();
                    list_temp.add(activity.getId());
                    map_SportId_ActivitiesId.put(activity.getSport_id(), list_temp);
                } 
            }
        }
      
        // On calcule la durée de chaque sport pratiqué
        HashMap<Integer,Double> map_SportId_duration= new HashMap<>();
        for(Integer id_sport : map_SportId_ActivitiesId.keySet()){
            map_sport.get(id_sport).setNbActivites(map_SportId_ActivitiesId.get(id_sport).size());
            int nb_activities = map_sport.get(id_sport).getNbActivites();
            max_nb_activity_sport = nb_activities > max_nb_activity_sport ? nb_activities : max_nb_activity_sport;
            min_nb_activity_sport = nb_activities < min_nb_activity_sport ? nb_activities : min_nb_activity_sport;
            for(Integer id_activity : map_SportId_ActivitiesId.get(id_sport)){
                if(map_SportId_duration.containsKey(id_sport)){
                    map_SportId_duration.replace(id_sport,map_SportId_duration.get(id_sport)+map_ActivityId_duration.get(id_activity));
                }
                else{
                    map_SportId_duration.put(id_sport, map_ActivityId_duration.get(id_activity));
                }
            }
        }
        map_SportId_ActivitiesId.clear();
        for(Integer i : map_SportId_duration.keySet()){
            Double duration_sport = map_SportId_duration.get(i);
            max_duration_sport = duration_sport > max_duration_sport ? duration_sport : max_duration_sport;
            min_duration_sport = duration_sport < min_duration_sport ? duration_sport : min_duration_sport;
        }
        // On récupère le sport le plus pratiqué
        Sport sport_most_practiced = HcQuery.getSportMostPracticed(user.getId());
        request.setAttribute("sport_most_practiced", sport_most_practiced);


        List<Activity> list_activity_no_archived = new ArrayList<>();
        List<Activity> list_activity_archived = new ArrayList<>();
        for(Activity act: list_activity){
            if(act.isArchive()){
                list_activity_archived.add(act);
            }
            else{
                list_activity_no_archived.add(act);
            }
        }
        // -- End --
        // Données de reference pour filter en js 
        HashMap<String,Double>  reference_duration = new HashMap<>();
        reference_duration.put("max_duration_activity", max_duration_activity);
        reference_duration.put("min_duration_activity",min_duration_activity);
        reference_duration.put("max_duration_sport", max_duration_sport);
        reference_duration.put("min_duration_sport",min_duration_sport);
        request.setAttribute("reference_duration", reference_duration);
        request.setAttribute("max_nb_activity_sport",max_nb_activity_sport);
        request.setAttribute("min_nb_activity_sport", min_nb_activity_sport);
        request.setAttribute("date_min_activity", date_min_activity);
        
        request.setAttribute("user", user);
        request.setAttribute("list_activity_archived", list_activity_archived);
        request.setAttribute("list_activity_no_archived", list_activity_no_archived);
        request.setAttribute("map_sport", map_sport);
        request.setAttribute("map_ActivityId_duration", map_ActivityId_duration);
        request.setAttribute("map_SportId_duration", map_SportId_duration);
        if(current_activity != null){
            request.setAttribute("current_activity", current_activity);
        }
        request.getRequestDispatcher("/jsp/home.jsp").forward(request, response);
    }
}