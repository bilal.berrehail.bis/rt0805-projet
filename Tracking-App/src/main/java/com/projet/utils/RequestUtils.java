package com.projet.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {

    public static void clearRequest(HttpServletRequest request) {
        request.getSession().invalidate();
        Enumeration<String> attribute_names = request.getAttributeNames();
        while (attribute_names.hasMoreElements()) {
            request.removeAttribute(attribute_names.nextElement());
        }
    }

    public static String getBaseUrl(HttpServletRequest request) {
        String scheme = request.getScheme() + "://";
        String serverName = request.getServerName();
        String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
        String contextPath = request.getContextPath();
        return scheme + serverName + serverPort + contextPath;
    }

    public static String requestPostAPI(HttpServletRequest request, String urlApi, Map<String, String> data)
            throws IOException {
        StringBuilder dataBody = new StringBuilder();

        for (String key : data.keySet()) {
            dataBody.append(key);
            dataBody.append('=');
            dataBody.append(data.get(key));
            dataBody.append('&');
        }

        HttpClient clientHttp = HttpClient.newHttpClient();
        HttpRequest requestHttp = HttpRequest.newBuilder().uri(URI.create(RequestUtils.getBaseUrl(request) + urlApi))
                .POST(HttpRequest.BodyPublishers.ofString(dataBody.toString())).setHeader("Content-type", "application/x-www-form-urlencoded").build();

        HttpResponse<String> responseHttp = null;
        try {
            responseHttp = clientHttp.send(requestHttp, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException e) {
            return "";
        }

        return responseHttp.body();
    }
}