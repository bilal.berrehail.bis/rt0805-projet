package com.projet.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.utils.RequestUtils;

@WebServlet(name = "Disconnect", urlPatterns = "/disconnect")
public class Disconnect extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestUtils.clearRequest(request);
        response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
    }
}