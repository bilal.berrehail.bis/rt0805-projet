package com.projet.utils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.projet.utils.constante.ConstanteRegister;

public class RegisterUtils implements ConstanteRegister {

    public static String checkFieldRegister(Map<String, String[]> fields) {
        String str = "valid";

        if (!fields.containsKey("email")) {
            return error_field_general + " " + error_field_null + "email";
        }
        if (!fields.containsKey("password")) {
            return error_field_general + " " + error_field_null + "password";
        }
        if (!fields.containsKey("confirm_password")) {
            return error_field_general + " " + error_field_null + "password confirmed";
        }
        if (!fields.containsKey("first_name")) {
            return error_field_general + " " + error_field_null + "first name";
        }
        if (!fields.containsKey("last_name")) {
            return error_field_general + " " + error_field_null + "last name";
        }
        if (!fields.containsKey("age")) {
            return error_field_general + " " + error_field_null + "age";
        }

        if (!isEmailAdress(fields.get("email")[0])) {
            return error_field_general + " " + error_email_invalidated;
        }

        if (fields.get("password")[0].length() < size_password) {
            return error_field_general + " " + error_password_size;
        }

        if ((fields.get("password")[0].length() != fields.get("confirm_password")[0].length())
                || !(fields.get("password")[0].equals(fields.get("confirm_password")[0]))) {
            return error_field_general + " " + error_password_not_same;
        }
        return str;
    }

    public static boolean isEmailAdress(String email) {
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
        Matcher m = p.matcher(email.toUpperCase());
        return m.matches();
    }
}