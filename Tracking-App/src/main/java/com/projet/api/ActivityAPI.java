package com.projet.api;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Sport;
import com.projet.token.TokenJwt;
import com.projet.utils.TokenUtils;

@WebServlet(name = "ActivityAPI", urlPatterns = "/api/activity")
public class ActivityAPI extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public class ResponseActivityAPI {
        String message;
        int activity_id;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");

        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        ResponseActivityAPI responseActivityAPI = new ResponseActivityAPI();

        if (tokenJwt == null) {
            responseActivityAPI.message = "Token incorrect";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("user_id", tokenJwt.getUser_id());
        conditions.put("is_progress", true);

        List<Activity> activities = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);

        if (activities == null || activities.size() == 0) {
            responseActivityAPI.message = "Récupération de l'activité en cours échoué";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        responseActivityAPI.message = "Récupération de l'activité en cours réussie";
        responseActivityAPI.activity_id = activities.get(0).getId();
        response.getWriter().println(new Gson().toJson(responseActivityAPI));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json; charset=UTF-8");

        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        ResponseActivityAPI responseActivityAPI = new ResponseActivityAPI();

        if (tokenJwt == null) {
            responseActivityAPI.message = "Token incorrect";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        String sport_idStr = request.getParameter("sport_id");
        String longitudeStr = request.getParameter("longitude");
        String latitudeStr = request.getParameter("latitude");

        if (sport_idStr == null || longitudeStr == null || latitudeStr == null) {
            responseActivityAPI.message = "Paramètres incorrects";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        Integer sport_id = Integer.valueOf(sport_idStr);
        Double longitude = Double.parseDouble(longitudeStr);
        Double latitude = Double.parseDouble(latitudeStr);

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("user_id", tokenJwt.getUser_id());
        conditions.put("is_progress", true);
        List<Activity> activities = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);

        if (activities != null) {
            if (activities.size() > 0) {
                responseActivityAPI.message = "Une activité est déjà en cours";
                responseActivityAPI.activity_id = -1;
                response.getWriter().println(new Gson().toJson(responseActivityAPI));
                return;
            }
        }

        conditions.clear();
        conditions.put("id", sport_id);
        List<Sport> sports = hc.getEntities(Sport.class, Sport.getEntityName(), conditions);

        if (sports == null) {
            responseActivityAPI.message = "Le sport précisé n'existe pas";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        Activity activity = new Activity();
        activity.setSport_id(sport_id);
        activity.setLongitude(longitude);
        activity.setLatitude(latitude);
        activity.setStart_date(new Date());
        activity.setProgress(true);
        activity.setUser_id(tokenJwt.getUser_id());

        hc.addEntity(activity);

        responseActivityAPI.message = "Création de l'activité réussie";
        responseActivityAPI.activity_id = activity.getId();
        response.getWriter().println(new Gson().toJson(responseActivityAPI));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");

        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        ResponseActivityAPI responseActivityAPI = new ResponseActivityAPI();
        if (tokenJwt == null) {
            responseActivityAPI.message = "Token incorrect";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        String act_id = request.getParameter("actv_id");
        if(act_id == null){
            responseActivityAPI.message = "Parametre activite id manquant" + request.getParameterMap().toString();
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        HibernateController hc = HibernateController.getInstance();
        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("id",Integer.valueOf(act_id));
        conditions.put("user_id",tokenJwt.getUser_id());
        List<Activity> activities = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);
        conditions.clear();

        if(activities == null || activities.size() == 0 ){
            responseActivityAPI.message = "L'activite n existe pas";
            responseActivityAPI.activity_id = -1;
            response.getWriter().println(new Gson().toJson(responseActivityAPI));
            return;
        }

        Activity activity = activities.get(0);
        activity.setArchive(true);
        hc.beginAndCommit();
        
        responseActivityAPI.message = "Activité archivé";
        responseActivityAPI.activity_id = activity.getId();
        response.getWriter().println(new Gson().toJson(responseActivityAPI));
    }
}