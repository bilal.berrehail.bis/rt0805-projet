package com.projet.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Tracking;

public class TrackingUtils {
    public static void generateGpx(Activity activity, String filePath) throws IOException {
        File file = new File(filePath);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(file);

        StringBuilder sb = new StringBuilder();

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<gpx version=\"1.0\">" + "<name>Example gpx</name>"
                + "<trk>" + "<trkseg>");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));

        sb.append(String.format("<trkpt lon=\"%s\" lat=\"%s\"><ele>50.0</ele><time>%s</time></trkpt>",
                activity.getLongitude(), activity.getLatitude(),
                df.format(activity.getStart_date())));

        List<Tracking> trackings = activity.getTrackings();

        for (int i = 0; i < trackings.size(); i++) {
            sb.append(String.format("<trkpt lon=\"%s\" lat=\"%s\"><ele>50.0</ele><time>%s</time></trkpt>",
                    trackings.get(i).getLongitude(), trackings.get(i).getLatitude(),
                    df.format(trackings.get(i).getDate())));
        }
        sb.append("</trkseg>" + "</trk>" + "</gpx>");

        fileOutputStream.write(sb.toString().getBytes());
        fileOutputStream.close();
    }
}