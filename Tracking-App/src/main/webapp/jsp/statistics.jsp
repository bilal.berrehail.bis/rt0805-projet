<%@ page contentType="text/html; ISO-8859-1; charset=UTF-8" language="java" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.lang.Math" %>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="com.projet.hibernate.model.Activity" %>
<%@ page import="com.projet.hibernate.model.Sport" %>
<%@ page import="com.projet.utils.StatisticUtils" %>
<%
     response.setHeader("Pragma", "no-cache");
     response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
     response.setHeader("Cache-Control", "pre-check=0, post-check=0");
%>

<html>

<head>
	<title>Display Statistics</title>
	<meta charset="UTF-8">

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link href="css/app.css" rel="stylesheet">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
     <script type="text/javascript" src="js/background.js"></script>
     <script type="text/javascript" src="js/statistics.js"></script>
     <link href="css/statistics.css" rel="stylesheet">
</head>

     <%
          Sport sport = (Sport)request.getAttribute("sport");
          List<Activity> activities = (List<Activity>)request.getAttribute("activities");
     %>
<jsp:include page="component/navbar.jsp" />  
<body>
     <img src="img/home.png" class="background"/> 
     <div class="container-global">
          <div class="first-vertical  div-flex">
               <h1 style="color: white; font-weight:bold; margin-top:20px; text-align:center; font-family: 'Roboto', sans-serif;">Sport : <span style="color: #e9ecef"><%= sport.getName() %></span></h1>
          </div>
          <div class="second-vertical div-flex">
               <div class="container-flex-column">
                    <div class="first-column">
                         <h3 style="color:white">Vos activités : </h3>
                         <table class="table table-striped table-bordered table-data table-dark" id="table-activity">
                              <thead>
                                   <tr>
                                        <th scope="col" >Numéro de l'activité</th>
                                        <th scope="col" >Durée</th>
                                        <th scope="col" >Distance parcourue (en km(s))</th>
                                        <th scope="col" >Vitesse moyenne (en km(s)/h)</th>
                                   </tr>
                              </thead>
                              <tbody>
                         <%
                              List<Double> durations = new ArrayList<>();
                              List<Double> distances = new ArrayList<>();
                              List<Double> vitesses = new ArrayList<>();

                              for (int i = 0; i < activities.size(); i++) {
                                   Double duration = StatisticUtils.getDurationOfActivity(activities.get(i));
                                   Double distance = StatisticUtils.getDistanceKm(activities.get(i));
                                   Double vitesse = StatisticUtils.roundTwoDecimals(distance / duration);

                                   durations.add(duration);
                                   distances.add(distance);
                                   vitesses.add(vitesse);
                                   %>
                                   <tr class="tr-data-activity" data-href="tracking?actv_id=<%=activities.get(i).getId()%>">
                                        <th scope="row"> <%= i %> </th>
                                        <td class="duration" duration="<%= duration %>"> </td>
                                        <td> <%= distance %> </td>
                                        <td> <%= vitesse %> </td>
                                   </tr>
                                   <%
                              }
                              Double sum_duration = 0.0, sum_distances = 0.0, average_vitesse = 0.0;

                              for (int i = 0; i < vitesses.size(); i++) {
                                   sum_duration += durations.get(i);
                                   sum_distances += distances.get(i);
                                   average_vitesse += vitesses.get(i);
                              }

                              average_vitesse /= vitesses.size();

                              %>
                              </tbody>
                         </table>
                    </div>
                    <div class="first-column">
                         <h3 style="color:white">Vos performances : </h3>
                         <table class="table table-bordered table-striped table-data  table-dark">
                              <thead>
                                   <tr>
                                   <th scope="row">Durée</td>
                                   <th scope="row">Nombre de km parcourus</td>
                                   <th  scope="row">Vitesse moyenne dans l'ensemble</td>
                                   </tr>
                              </thead>
                              <tbody>
                                   <tr>
                                        <td class="duration" duration="<%= sum_duration %>"></td>
                                        <td> <%= sum_distances %> <span style="font-weight:bold">km(s)</span> </td>
                                        <td> <%= StatisticUtils.roundTwoDecimals(average_vitesse) %> <span style="font-weight:bold">km(s)/heure</span> </td>
                                   </tr>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</body>

<style>
     table{

}
</style>

</html>