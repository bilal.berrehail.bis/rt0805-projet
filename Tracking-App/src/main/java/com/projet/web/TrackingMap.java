package com.projet.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Tracking;
import com.projet.token.TokenJwt;
import com.projet.utils.RequestUtils;
import com.projet.utils.TokenUtils;
import com.projet.utils.TrackingUtils;
import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Sport;

@WebServlet(name = "TrackingMap", urlPatterns = "/tracking")
public class TrackingMap extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // -- Vérifications --
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);
        if (tokenJwt == null) {
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
            return;
        }

        if (request.getParameter("actv_id") == null) {
            response.getWriter().println("pas de actv_id précisé");
            return;
        }

        Integer actv_id = Integer.valueOf(request.getParameter("actv_id"));

        HibernateController hc = HibernateController.getInstance();

        // -- Récupération de l'activité --
        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("id", actv_id);
        conditions.put("user_id", tokenJwt.getUser_id());

        List<Activity> activities = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);
        if (activities == null || activities.size() == 0) {
            response.getWriter().println("Vous n'avez pas d'activité qui porte cet identifiant");
            return;
        }

        // -- Récupération des trackings --
        conditions.clear();
        conditions.put("activity_id", actv_id);

        List<Tracking> trackings = hc.getEntities(Tracking.class, Tracking.getEntityName(), conditions);
        if (trackings == null || trackings.size() == 0) {
            response.getWriter().println("Aucun point de tracking sur cette activité");
            return;
        }
        activities.get(0).setTrackings(trackings);

        // -- Récupération du sport --
        conditions.clear();
        conditions.put("id", activities.get(0).getSport_id());
        List<Sport> sports = hc.getEntities(Sport.class, Sport.getEntityName(), conditions);
        if (sports.size() == 0) {
            response.getWriter().println("Aucun sport lié à l'activité n'a été trouvé");
            return;
        }

        // -- Génération du fichier GPX --
        String filePath = getServletConfig().getServletContext().getRealPath("/moisson.gpx");
        TrackingUtils.generateGpx(activities.get(0), filePath);

        // -- End --
        request.setAttribute("sport", sports.get(0));
        request.setAttribute("activity", activities.get(0));
        request.setAttribute("gpx_filename", "moisson.gpx");
        request.getRequestDispatcher("/jsp/tracking.jsp").forward(request, response);
    }
}
