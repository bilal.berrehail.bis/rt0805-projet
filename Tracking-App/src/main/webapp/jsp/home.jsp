
<%@ page contentType="text/html; charset=UTF-8" language="java"%> 
<%@ page import="com.projet.hibernate.model.Activity" %>
<%@ page import="com.projet.hibernate.model.User" %>
<%@ page import="com.projet.hibernate.model.Sport" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%
     response.addHeader("Cache-Control", "pre-check=0, post-check=0");
%>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

<!-- Datepicker -->
<link href='js/js_utils/datepicker-master/assets/css/bootstrap-datepicker.css' rel='stylesheet' type='text/css'>
<script src='js/js_utils/datepicker-master/assets/js/bootstrap-datepicker.js' type='text/javascript'></script>
<!-- selectpicker -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.js' type='text/javascript'></script>
	<link href="css/home.css" rel="stylesheet">
	<link href="css/app.css" rel="stylesheet">
	<script type="text/javascript" src="js/home.js"></script>
	<script type="text/javascript" src="js/form.js"></script>
	<script type="text/javascript" src="js/background.js"></script>
  </head>
  <body>
        <%
        User user = (User) request.getAttribute("user");
		Sport sport_most_practiced = (Sport) request.getAttribute("sport_most_practiced");
		String phrase_sport_most_practiced = sport_most_practiced == null ? "Sport favori : Vous n'avez pas encore de sport favori" : "Sport favori : "+sport_most_practiced.getName();
		HashMap<Integer,Double> map_SportId_duration = (HashMap<Integer,Double>) request.getAttribute("map_SportId_duration");
		HashMap<Integer,Sport> map_sport = (HashMap<Integer,Sport>) request.getAttribute("map_sport");
		
		Activity current_activity = (Activity) request.getAttribute("current_activity");
        List<Activity> list_activity_no_archived = (List<Activity>) request.getAttribute("list_activity_no_archived");
		List<Activity> list_activity_archived = (List<Activity>) request.getAttribute("list_activity_archived");
        HashMap<Integer,Double> map_ActivityId_duration = (HashMap<Integer,Double>) request.getAttribute("map_ActivityId_duration");

		// Données de references pour filtrer 
		HashMap<String,Double> reference_duration = (HashMap<String,Double>) request.getAttribute("reference_duration");
        int max_nb_activity_sport = (int) request.getAttribute("max_nb_activity_sport");
        int min_nb_activity_sport = (int) request.getAttribute("min_nb_activity_sport");
        Date date_min_activity = (Date) request.getAttribute("date_min_activity");
        %>
		<img src="img/home.png" class="background"/> 
           <div class="container">
		<div class="first header"> 
			<div class="nav_left">
  				<div class="div_nav_petit">
	  				<div id="div_home" class="div_nav">
	  					<button id="btn_home" class="btn btn_nav">
	  						<i id="i_home" class="fa i_btn"></i>
	  					</button>
	  					<span class="span_btn" id="home" bold><strong>Home</strong></span>
	  				</div>
  				</div>
  				<div class="div_nav_grand">

  				</div>
  			</div>
  			<div class="nav_right">
  				<div class="div_nav_grand">

  				</div>
  				<div class="div_nav_petit">
  					<div style="margin-right: 10%">Bienvenue <strong><%=user.getFirst_name()+" "+user.getName()%></strong> </div>
	  				<div id="div_disconnect" class="div_nav">
	  					<button id="btn_disconnect" class="btn btn_nav">
	  						<i id="i_disconnect" class="fa i_btn"></i>
	  					</button>
	  					<span class="span_btn" id="disconnect"><strong>Disconnect</strong></span>
						  <script> 
							let $disconnect_div = $('#div_disconnect');
							$disconnect_div.click( function(event){
								console.log("disconnect clicked");
								window.location.href = 'disconnect'
							});	
							let $back_to_home_div = $('#div_home');
							$back_to_home_div.click(function(event){
								console.log("home clicked");
								window.location.href = 'home'
							});
						</script>
	  				</div>
  				</div>
  			</div>
		</div>

		<div class="second body">
			<!-- ici on a rajouté trois div -->
			<div class="div-body div_left">
				<div class="signup-form">           
            		<form id="form-info">
		                <h3>Vos informations</h3>
		                <div class="form-group">
		                    <div class="row">
		                        <div class="col-xs-6"><input id="inp_first_name" type="text" class="form-control inp_info_user" name="first_name" value="<%=user.getFirst_name()%>" disabled></div>
		                        <div class="col-xs-6"><input id="inp_last_name" type="text" class="form-control inp_info_user" name="last_name" value="<%=user.getName()%>" disabled></div>
		                    </div>        	
		                </div>
		                <div class="form-group">
		                    <input id="inp_age" type="number" class="form-control inp_info_user" placeholder="Age" name="age" value="<%=user.getAge()%>" min="13" max="130"  disabled>
		                </div>
		                <div class="form-group">
		                    <input id="inp_email" type="email" class="form-control inp_info_user" name="email" value="<%=user.getEmail()%>" disabled>
		                </div>
		                <div class="form-group">
		                    	<input type="text" class="form-control" name="favorite_sport" value="<%=phrase_sport_most_practiced%>" disabled>
		                </div>
		                <div class="form-group" id="div_btn_success">
                    		<button id="btn-modify-info" type="submit" class="btn btn-primary btn-lg btn-block">Modifier mes informations</button>
                    	</div>
	                </form>
				</div>
			</div>
			<div class="div-body div-middle">
				<div id="container-sport" class="signup-form">           
            		<form id="form-sport">
		                <h3 id="title-form-sport">Vos Sports pratiqués</h3>
						<p class="hint-text div-data-sport">Cliquez sur un sport pour voir ses détails</p>
		                <div id="container-list-sport" class="form-group container-table div-data-sport">
		                  	<table id="table-list-sport" class="table table-list">
							  <thead class="thead-dark">
							    <tr>
							      <th id="ref_sport" max_duration="<%=reference_duration.get("max_duration_sport")%>" min_duration="<%=reference_duration.get("min_duration_sport")%>" max_nb_activity="<%=max_nb_activity_sport%>" min_nb_activity="<%=min_nb_activity_sport%>" scope="col">#</th>
							      <th scope="col">Sport</th>
							      <th scope="col">Nombre d'activités</th>
							      <th scope="col">Durée totale</th>
								  <th scope="col">Action</th>
							    </tr>
							  </thead>
							  <tbody>
                              <% 
                              if(map_SportId_duration != null){
                                for( Integer sportId: map_SportId_duration.keySet()){
									%>
                                    <tr class='line-data-sports'>
                                    <th class="sport-id" scope="row"><%=sportId%></th>
                                    <td class="sport-name"><%=map_sport.get(sportId).getName()%> </td>
                                    <td class="sport-nb-act"><%=map_sport.get(sportId).getNbActivites()%> </td>
                                    <td class="sport-duration data-duration" duration="<%=map_SportId_duration.get(sportId)%>"></td>
									<td class="sport-action td-action"><i class="icon-action icon-action-more"  data-href='statistics?sport_id=<%=sportId%>' data-toggle="tooltip" data-placement="top" title="Consulter vos statistiques"></i></td>
                                    </tr>
                                    <%
                                }
                              }
                              %>
							  </tbody>
							</table>
						</div>
						<div class="form-group div-btn-form div-data-sport" id="div-btn-sport">
                    		<button id="btn-sport-filter" type="submit" class="btn btn-primary btn-lg btn-block">Filtrer</button>
                    	</div>
					</form>
				</div>
			</div>
			<div class="div-body div-right">
				<div id="container-activity" class="signup-form">           
            		<form id="form-activity">
		                <h3 id="title-form-activity">Vos Activités</h3>
						<%	
						if(current_activity != null){
						%><p class="hint-text div-data-activity data-current-activity">Votre activité en cours</p>
		                <div id="container-current-activity" class="form-group div-data-activity data-current-activity">
		                  	<table id="table-sport" class="table" id="table-current-act">
							  <thead class="thead-dark">
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Sport</th>
							      <th scope="col">Durée (heure)</th>
							      <th scope="col">Date</th>
							    </tr>
							  </thead>
							  <tbody>
                                    <tr>
                                    <th scope="row"><%=current_activity.getId()%> </th>
                                    <td><%=map_sport.get(current_activity.getSport_id()).getName()%> </td>
                                    <td><%="En cours"%> </td>
                                    <td><%=new SimpleDateFormat("dd/MM/yyyy").format(current_activity.getStart_date())%> </td>
                                    </tr>
                                    <%
                              %>
							  </tbody>
							</table>
		                </div><%
						}
						%>
						<p class="hint-text div-data-activity">Cliquez sur une activité pour voir ses details.</p>
		                <div id="container-list-activity" class="form-group div-data-activity">
		                  	<table id="table-activity" class="table" id="table-all-act">
							  <thead class="thead-dark">
							    <tr>
							      <th id="ref_activity" date_min="<%=new SimpleDateFormat("MM/dd/yyyy").format(date_min_activity)%> "max_duration="<%=reference_duration.get("max_duration_activity")%>" min_duration="<%=reference_duration.get("min_duration_activity")%>" scope="col">#</th>
							      <th scope="col">Sport</th>
							      <th scope="col">Durée (heure)</th>
							      <th scope="col">Date</th>
								  <th scope="col">Actions</th>
							    </tr>
							  </thead>
							  <tbody>
                              <% 
                              if(list_activity_no_archived != null){
                                for(Activity act : list_activity_no_archived){ %>
                                    <tr class='activity-no-archived''>
                                    <th class="activity-id" scope="row"><%=act.getId()%></th>
                                    <td class="activity-sport" sport-id="<%=act.getSport_id()%>"><%=map_sport.get(act.getSport_id()).getName()%> </td>
                                    <td class="activity-duration data-duration" duration="<%=map_ActivityId_duration.get(act.getId())%>"></td>
                                    <td class="activity-date" data-date="<%=new SimpleDateFormat("MM/dd/yyyy").format(act.getStart_date())%>"><%=new SimpleDateFormat("dd/MM/yyyy").format(act.getStart_date())%> </td>
									<td class="activity-action td-action"><i class="icon-action icon-action-more"  data-href='tracking?actv_id=<%=act.getId()%>' data-toggle="tooltip" data-placement="top" title="Consulter vos statistiques"></i><span class="span-btw-icon"></span><i class="icon-action icon-action-delete"  data-toggle="tooltip" data-placement="top" title="Archivez l'activité">
                                    </tr>
                                    <%
                                }
                              }
							  if(list_activity_archived != null){
                                for(Activity act : list_activity_archived){ %>
                                    <tr class='activity-archived' data-href='tracking?actv_id=<%=act.getId()%>' hidden>
                                    <th class="activity-id"  scope="row"><%=act.getId()%> </th>
                                    <td class="activity-sport" sport-id="<%=act.getSport_id()%>"><%=map_sport.get(act.getSport_id()).getName()%> </td>
                                    <td class="activity-duration data-duration" duration="<%=map_ActivityId_duration.get(act.getId())%>"></td>
                                    <td class="activity-date" data-date="<%=new SimpleDateFormat("MM/dd/yyyy").format(act.getStart_date())%>"><%=new SimpleDateFormat("dd/MM/yyyy").format(act.getStart_date())%> </td>
                                    </tr>
                                    <%
                                }
                              }
                              %>
							  </tbody>
							</table>
		                </div>
		                <div class="form-group div-btn-form div-data-activity" id="div-btn-activity" class="">
                    		<button type="submit" id="btn-activity-filter" class="btn btn-primary btn-lg btn-block">Filtrer</button>
                    	</div>
	                </form>
                </div>
		 	</div>
		</div>
		<div class="third footer">
			<div class ="div-third footer">
				</div>
			</div>
		</div>
	</div>
  </body>
</html>
         