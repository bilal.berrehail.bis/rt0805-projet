package com.projet.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "user")
@Table(name = "user")
public class User implements HibernateModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, unique = true)
	private int id;

	@Column(name= "email", nullable=false)
	private String email;

	@Column(name = "name", nullable = false)
    private String name;
 
	@Column(name = "first_name", nullable = false)
	private String first_name;
		
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "age", nullable=false)
	private int age;

	public User() {
		
	}
	
	public User(String name, String first_name, String password, String email, int age) {
		this.name = name;
		this.first_name = first_name;
		this.password = password;
		this.email = email;
		if(age > -1 && age < 130){
			this.age = age;
		}
	}
	
	public String toString(){
		return " | id : "+this.id
				+" | name : "+this.name
				+" | firstname : "+this.first_name
				+" | email :"+this.email
				+" | password : "+this.password
				+" | age : "+this.age;
	}
	public static String getEntityName() {
		return "user";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
