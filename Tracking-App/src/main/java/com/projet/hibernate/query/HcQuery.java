package com.projet.hibernate.query;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Sport;

public class HcQuery {

    public static Sport getSportMostPracticed(int user_id) {
        HibernateController hc = HibernateController.getInstance();

        EntityManager entityManager = hc.getEntityManager();

        String query = "SELECT sport_id, MAX(count_res) count FROM (SELECT sport_id, COUNT(*) count_res FROM activity a2 WHERE a2.user_id = :id GROUP BY a2.sport_id) a";

        Query querySql = entityManager.createNativeQuery(query);
        querySql.setParameter("id", user_id);

        List<Object[]> result = querySql.getResultList();

        if (result.size() > 0) {
            if (result.get(0).length > 0) {
                if (result.get(0)[0] != null) {
                    int sport_id = Integer.valueOf(result.get(0)[0].toString());

                    HashMap<String, Object> conditions = new HashMap<>();
                    conditions.put("id", sport_id);
    
                    List<Sport> sport = hc.getEntities(Sport.class, Sport.getEntityName(), conditions);
    
                    if (sport == null || sport.size() == 0) {
                        return null;
                    }
    
                    return sport.get(0);
                }
            }
        }

        return null;
    }
}