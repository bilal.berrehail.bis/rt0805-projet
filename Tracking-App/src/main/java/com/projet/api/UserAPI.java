package com.projet.api;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.User;
import com.projet.token.TokenJwt;
import com.projet.utils.HashUtils;
import com.projet.utils.TokenUtils;
import com.projet.utils.constante.ConstanteToken;
import com.projet.utils.RegisterUtils;

@WebServlet(name = "UserAPI", urlPatterns = "/api/user")
public class UserAPI extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HashMap<String, String> resultJson = new HashMap<>();

        response.setContentType("application/json; charset=UTF-8");

        TokenJwt tokenJwt = TokenUtils.verifyToken(request);
        if (tokenJwt == null) {
            resultJson.put("code", "0");
            resultJson.put("msg", "Token incorrect");
            response.getWriter().println(new Gson().toJson(resultJson));
            return;
        }

        String new_first_name = request.getParameter("first_name");
        String new_name = request.getParameter("last_name");
        String new_age = request.getParameter("age");
        String new_email = request.getParameter("email");
        String password = request.getParameter("password");
        String new_password = request.getParameter("new_password");
        String new_password_confirm = request.getParameter("confirm_password");

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("id", tokenJwt.getUser_id());
        List<User> users = hc.getEntities(User.class, User.getEntityName(), conditions);
        if (users == null || users.size() == 0) {
            resultJson.put("code", "0");
            resultJson.put("msg", ConstanteToken.error_user_token);
        }
        if (password == null) {
            resultJson.put("code", "0");
            resultJson.put("msg", "Rentrez votre mot de passe");
        } else if (new_password != null && new_password_confirm == null) {
            resultJson.put("code", "0");
            resultJson.put("msg", RegisterUtils.error_field_null + " confirmer le password");
        } else if (new_password != null && new_password.length() < RegisterUtils.size_password) {
            resultJson.put("code", "0");
            resultJson.put("msg", RegisterUtils.error_password_size);
        } else if (new_password != null && !new_password.equals(new_password_confirm)) {
            resultJson.put("code", "0");
            resultJson.put("msg", RegisterUtils.error_new_password_not_same);
        } else if (new_email != null && !RegisterUtils.isEmailAdress(new_email)) {
            resultJson.put("code", "0");
            resultJson.put("msg", RegisterUtils.error_email_invalidated + " " + new_email);
        } else {
            try {
                String password_decrypted = HashUtils.sha256encrypt(password);
                if (!password_decrypted.equals(users.get(0).getPassword())) {
                    resultJson.put("code", "0");
                    resultJson.put("msg", RegisterUtils.error_password_not_correct);
                } else {
                    if (new_password != null) {
                        users.get(0).setPassword(HashUtils.sha256encrypt(new_password));
                    }
                    if (new_first_name != null) {
                        users.get(0).setFirst_name(new_first_name);
                    }
                    if (new_name != null) {
                        users.get(0).setName(new_name);
                    }
                    if (new_age != null) {
                        users.get(0).setAge(Integer.valueOf(new_age));
                    }
                    if (new_email != null) {
                        users.get(0).setEmail(new_email);
                    }
                    hc.beginAndCommit();
                    resultJson.put("code", "1");
                    resultJson.put("msg", "Changement(s) effectué(s)");
                }
            } catch (NoSuchAlgorithmException e) {
                resultJson.put("code", "0");
                resultJson.put("msg", "L'algorithme utilisé est down");
            }
        }
        response.getWriter().println(new Gson().toJson(resultJson));

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HashMap<String, String> resultJson = new HashMap<>();

        response.setContentType("application/json; charset=UTF-8");

        if (request.getParameter("email") == null || request.getParameter("password") == null) {
            resultJson.put("code", "0");
            resultJson.put("msg", "Champs incorrects");
            response.getWriter().println(new Gson().toJson(resultJson));
            return;
        }

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("email", email);
        try {
            conditions.put("password", HashUtils.sha256encrypt(password));

            List<User> users = hc.getEntities(User.class, User.getEntityName(), conditions);
            if (users == null || users.size() == 0) {
                resultJson.put("code", "0");
                resultJson.put("msg", "Identifiants incorrects");
            } else {
                TokenJwt tokenJwt = new TokenJwt(users.get(0).getId());
                String token_api = TokenUtils.createApiToken(tokenJwt);

                resultJson.put("code", "1");
                resultJson.put("msg", "Connexion réussie");
                resultJson.put("token", token_api);
            }
        } catch (NoSuchAlgorithmException e) {
            resultJson.put("code", "0");
            resultJson.put("msg", "L'algorithme utilisé est down");
        }

        response.getWriter().println(new Gson().toJson(resultJson));
    }
}