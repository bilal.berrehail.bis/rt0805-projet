package com.projet.utils;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import com.projet.token.TokenJwt;

import io.jsonwebtoken.ExpiredJwtException;

import java.security.Key;
import java.util.Date;

import com.google.gson.Gson;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.InvalidKeyException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import io.jsonwebtoken.security.WeakKeyException;

import com.projet.utils.constante.ConstanteToken;

public class TokenUtils implements ConstanteToken {
    private static final String key = "#dF(gd23,f:45456!:?NU2sd:m!:d:miez,f,;@lù";
    private static final Long time_token_validity = 2678400000L;

    public static TokenJwt verifyToken(HttpServletRequest request)
            throws ExpiredJwtException, IllegalArgumentException, UnsupportedEncodingException {
        if (request.getHeader("token") != null) {
            if (!request.getHeader("token").equals("")) {
                return createTokenFromApi(request.getHeader("token"));
            }
        }

        if (request.getSession().getAttribute("token_api") == null) {
            return null;
        }

        String validToken = checkToken(request.getSession().getAttribute("token_api").toString());
        if (!validToken.equals("valid")) {
            return null;
        }
        return createTokenFromApi(request.getSession().getAttribute("token_api").toString());
    }

    public static Key getKey() throws WeakKeyException, UnsupportedEncodingException {
        try {
            return Keys.hmacShaKeyFor(key.getBytes("UTF-8"));
        } catch (WeakKeyException e) {
            System.out.println("The Key used to encode the token is too weak. Choose another");
            return null;
        } catch (UnsupportedEncodingException e) {
            System.out.println("The encoding format choosed to encode the token is incorrect");
            return null;
        }
    }

    public static String createApiToken(TokenJwt tokenJwt)
            throws WeakKeyException, InvalidKeyException, UnsupportedEncodingException {
        Gson gson = new Gson();
        return Jwts.builder().setExpiration(new Date(System.currentTimeMillis() + time_token_validity))
                .setSubject(gson.toJson(tokenJwt)).signWith(getKey()).compact();
    }

    public static String checkToken(String token)
            throws IllegalArgumentException, UnsupportedEncodingException, ExpiredJwtException {
        try {
            Jwts.parserBuilder().setSigningKey(getKey()).build().parseClaimsJws(token);
            return "valid";
        } catch (ExpiredJwtException jwt) {
            return error_token_perime;
        } catch (JwtException e) {
            return error_token_incorrect;
        }
    }

    public static TokenJwt createTokenFromApi(String token)
            throws WeakKeyException, SignatureException, ExpiredJwtException, UnsupportedJwtException,
            MalformedJwtException, IllegalArgumentException, UnsupportedEncodingException {
        String s = Jwts.parserBuilder().setSigningKey(getKey()).build().parseClaimsJws(token).getBody().getSubject();
        Gson gson = new Gson();
        TokenJwt tokenJwt = gson.fromJson(s, TokenJwt.class);
        return tokenJwt;
    }
}