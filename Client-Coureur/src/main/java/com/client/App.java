package com.client;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class App {
    public static final String baseUrl = "http://127.0.0.1:8080/Tracking-App/";
    public static String token = "";
    public static Integer activity_id = -1;

    public class Sport {
        int id;
        String name;
    }
    
    public class ResponseSportAPI {
        String message;
        List<Sport> sports;
    }

    public class ResponseActivityAPI {
        String message;
        int activity_id;
    }

    public class ResponseTrackingAPI {
        String message;
    }

    public static void main(String[] args) throws IOException
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Entrez votre email :");
        String email = keyboard.nextLine();
        System.out.println("Entrez votre mot de passe :");
        String password = keyboard.nextLine();
        login(email, password);
        if (!token.equals("")) {


            getActivityInProgress();

            if (activity_id == -1) {
                List<Sport> sports = getSports();
                if (sports != null) {
                    System.out.println("Choisissez le sport dans lequel vous souhaitez lancer une activité :");
                    for (Sport sport : sports) {
                        System.out.println(String.format("[%d] : %s", sport.id, sport.name));
                    }
    
                    int sport_id = keyboard.nextInt();
    
                    System.out.println("Entrez la latitude de départ :");
                    Double latitude = keyboard.nextDouble();
    
                    System.out.println("Entrez la longitude de départ :");
                    Double longitude = keyboard.nextDouble();
    
                    pushActivity(sport_id, latitude, longitude);
                }
            }
            
            if (activity_id == -1) {
                System.out.println("Impossible de créer une nouvelle activité ou de récuperer une activité en progression");
                keyboard.close();
                return;
            }

            boolean isFinished = false;

            System.out.println("Push des points GPS pour le tracking :");
    
            while (!isFinished) {
                System.out.println("Entrez la latitude dans laquelle vous vous trouvez :");
                Double latitude = keyboard.nextDouble();
    
                System.out.println("Entrez la longitude dans laquelle vous vous trouvez :");
                Double longitude = keyboard.nextDouble();

                System.out.println("Etait-ce votre dernière balise ? (0 = Non | 1 = Oui)");
                int end = keyboard.nextInt();
                isFinished = end == 0 ? false : true;
    
                pushTracking(activity_id, latitude, longitude, isFinished);
            }

            keyboard.close();
        }
    }

    public static List<Sport> getSports() throws IOException {
        String result = requestGetAPI("api/sport", new HashMap<String, String>());

        if (result == null) {
            System.out.println("Impossible d'accéder à l'API : api/sport");
            return null;
        }

        ResponseSportAPI responseSportAPI = new Gson().fromJson(result, ResponseSportAPI.class);
        System.out.println("[Sport Response] message : " + responseSportAPI.message);

        return responseSportAPI.sports;
    }

    public static void getActivityInProgress() throws IOException {
        String result = requestGetAPI("api/activity", new HashMap<String, String>());

        if (result == null) {
            System.out.println("Impossible d'accéder à l'API : api/activity");
            return;
        }

        ResponseActivityAPI responseActivityAPI = new Gson().fromJson(result, ResponseActivityAPI.class);
        System.out.println("[Activity Response] message : " + responseActivityAPI.message);

        activity_id = responseActivityAPI.activity_id;
    }

    public static void pushActivity(Integer sport_id, Double latitude, Double longitude) throws IOException {
        HashMap<String, String> data = new HashMap<>();
        data.put("sport_id", sport_id.toString());
        data.put("latitude", latitude.toString());
        data.put("longitude", longitude.toString());

        String result = requestPostAPI("api/activity", data);

        if (result == null) {
            System.out.println("Impossible d'accéder à l'API : api/activity");
            return;
        }

        ResponseActivityAPI responseActivityAPI = new Gson().fromJson(result, ResponseActivityAPI.class);
        System.out.println("[Activity Response] message : " + responseActivityAPI.message);

        activity_id = responseActivityAPI.activity_id;
    }

    public static void pushTracking(Integer actv_id, Double latitude, Double longitude, boolean isEnd) throws IOException {
        if (actv_id == -1) {
            System.out.println("Aucune activité n'a été entrée");
            return;
        }

        HashMap<String, String> data = new HashMap<>();
        data.put("actv_id", actv_id.toString());
        data.put("latitude", latitude.toString());
        data.put("longitude", longitude.toString());
        if (isEnd) {
            data.put("end", "1");
        }

        String result = requestPostAPI("api/tracking", data);

        if (result == null) {
            System.out.println("Impossible d'accéder à l'API : api/tracking");
            return;
        }

        ResponseTrackingAPI responseTrackingAPI = new Gson().fromJson(result, ResponseTrackingAPI.class);
        System.out.println("[Tracking Response] message : " + responseTrackingAPI.message);
    }

    public static void login(String email, String password) throws IOException {
        HashMap<String, String> postDataLogin = new HashMap<>();
        postDataLogin.put("email", email);
        postDataLogin.put("password", password);
        String responsesBody = requestPostAPI("api/user", postDataLogin);
        
        Type type = new TypeToken<Map<String, String>>() {}.getType();
        if (responsesBody.equals("")) {
            System.out.println("Impossible d'interroger l'API");
            return;
        }

        Map<String, String> responseDecode = new Gson().fromJson(responsesBody, type);

        if (responseDecode.get("code").equals("0")) {
            System.out.println("Message d'erreur : " + responseDecode.get("msg"));
        } else {
            String getToken = responseDecode.get("token");
            System.out.println("Connexion réussie !");
            token = getToken;
        }
    }

    public static String requestGetAPI(String urlAPI, Map<String, String> data) throws IOException {
        HttpClient clientHttp = HttpClient.newHttpClient();

        HttpRequest requestHttp = HttpRequest.newBuilder().uri(URI.create(baseUrl + urlAPI)).setHeader("token", token).build();

        HttpResponse<String> responseHttp = null;
        try {
            responseHttp = clientHttp.send(requestHttp, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException e) {
            return null;
        }

        return responseHttp.body();
    }

    public static String requestPostAPI(String urlApi, Map<String, String> data)
            throws IOException {
        StringBuilder dataBody = new StringBuilder();

        for (String key : data.keySet()) {
            dataBody.append(key);
            dataBody.append('=');
            dataBody.append(data.get(key));
            dataBody.append('&');
        }

        HttpClient clientHttp = HttpClient.newHttpClient();
        HttpRequest requestHttp = HttpRequest.newBuilder().uri(URI.create(baseUrl + urlApi))
                .POST(HttpRequest.BodyPublishers.ofString(dataBody.toString())).setHeader("token", token).setHeader("Content-type", "application/x-www-form-urlencoded").build();

        HttpResponse<String> responseHttp = null;
        try {
            responseHttp = clientHttp.send(requestHttp, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException e) {
            return null;
        }

        return responseHttp.body();
    }
}
