<%@ page contentType="text/html; charset=UTF-8" language="java"%> 
<%@ page import="com.projet.utils.constante.ConstanteRegister" %>
<%
    response.addHeader("Cache-Control", "pre-check=0, post-check=0");
    int password_size_min = ConstanteRegister.size_password;
%>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
            <title>Inscription</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="js/form.js"></script>
            <script type="text/javascript" src="js/background.js"></script>
            <link rel="stylesheet" href="css/form.css" />
            <link href="css/app.css" rel="stylesheet">
        </head>
        <body>
        <img src="img/register.webp" class="background"/> 
        <div id="main-container">
            <div class="signup-form">
                <form id="form_register" action="register" method="post">
                    <h2>Inscription</h2>
                    <p class="hint-text">Créer votre compte facilement, et gratuitement.</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6"><input id="inp_first_name" type="text" class="form-control" name="first_name" placeholder="Prénom" required="required"></div>
                            <div class="col-xs-6"><input id="inp_last_name" type="text" class="form-control" name="last_name" placeholder="Nom de famille" required="required"></div>
                        </div>        	
                    </div>
                    <div class="form-group">
                        <input id="inp_age" type="number" class="form-control" placeholder="Age" name="age" placeholder="20" min="13" max="130"  required="required">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Mot de passe  -- <%=password_size_min%> caractères minimum --" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirmer le mot de passe" required="required">
                    </div>        
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox" required="required"> En cliquant sur ce bouton, j'accepte de partager mes informations personnelles et mes données de géo-localisation </label>
                    </div>
                    <%
                    String first_name = (String) request.getAttribute("first_name");
                    if( first_name != null){
                    %> 
                        <script> $( '#inp_first_name' ).val('<%=first_name%>');</script>
                    <%
                    }
                    String last_name = (String) request.getAttribute("last_name");
                    if( last_name != null){
                    %> 
                        <script> $( '#inp_last_name' ).val('<%=last_name%>');</script>
                    <%
                    }
                    String age = (String) request.getAttribute("age");
                    if( age != null){
                        %> 
                        <script> $( '#inp_age' ).val('<%=age%>');</script>
                    <%
                    }
                    String error = (String) request.getAttribute("error");
                    if(error != null){
                        request.removeAttribute("error");
                        %> 
                        <script>
                            let new_node = $("<div></div>")
                                .attr("class","text-center div_p_info");
                            let new_p = $("<p></p>")
                                .text('<%=error%>')
                                .attr("class","p_error");
                            $(new_node).append($(new_p));
                            $( "#form_register" ).append( $(new_node) );
                        </script>
                    <%
                    }
                    %>
                    <div class="form-group" id="div_btn_success">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">S'inscrire</button>
                    </div>
                </form>
                <div class="text-center">Vous avez déja un compte ? <a href="login">Se connecter</a></div>
            </div>
        </div> 
        </body>
    </head>
</html>