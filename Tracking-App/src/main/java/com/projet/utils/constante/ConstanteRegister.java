package com.projet.utils.constante;

public interface ConstanteRegister {
    final int size_password = 1;

    final String error_field_general = "Erreur lors de la vérification des champs";
    final String error_field_null = "Le champ ne doit pas être null : ";
    final String error_email_invalidated = "Format d'email incorrect";
    final String error_email_used = "Email déja utilisé";
    final String error_password_size = "Taille minimale du mot de passe non respecté : " + size_password + " caractères";
    final String error_password_not_same = "Les mot de passes ne concordent pas";
    final String succeed_user_created = "Le compte utilisateur a bien été créé";
    final String error_login_incorrect = "Erreur aucun compte trouvé avec cet email ou le mot de passe est incorrect";
    final String error_new_password_not_same = "Les nouveaux mot de passes ne concordent pas";
    final String error_password_not_correct = "Le mot de passe n'est pas correct";
}