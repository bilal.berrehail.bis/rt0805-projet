$( document ).ready(function() {
    // On affiche la durée au format 'heure' h 'minute'
    $td_duration = $('.duration');
    $($td_duration).each(function(){
        let duration = $(this).attr('duration');
        console.log(duration);
        $(this).text(hour_from_double(duration));
    })

    // On bind le click et on renvoie sur la page trackings
    $(".tr-data-activity").click(function() {
        window.location = $(this).data("href");
    });
});


function hour_from_double(time){
    let decimal_part_hour = time % 1 ; 
    let hour = time - decimal_part_hour;
    let minute = decimal_part_hour * 60 ;
    let decimal_part_minute = minute % 1 ; 
    minute = minute - decimal_part_minute;
    if(minute==0){
        minute="00"
    }
    else{
        minute = minute.toString(10)
    }
    result = hour.toString(10)+" h "+minute;
    console.log(decimal_part_hour,hour,minute,decimal_part_minute,minute,result);
    return result;
}