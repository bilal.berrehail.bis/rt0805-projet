<%@ page contentType="text/html; charset=UTF-8" language="java"%> 

<%
     response.addHeader("Cache-Control", "pre-check=0, post-check=0");
%>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
            <title>Connexion</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="js/form.js"></script> 
            <script type="text/javascript" src="js/background.js"></script>
            <link rel="stylesheet" href="css/form.css" />
            <link href="css/app.css" rel="stylesheet">

            <%
                String email_prefilled = (String)request.getAttribute("email_prefilled");
                String success_message = (String)request.getAttribute("success_message");
                String error_message = (String)request.getAttribute("error_message");
            %>

        </head>
        <body>
        <img src="img/register.webp" class="background"/> 
        <div id=main-container>
            <div class="signup-form">           
                <form id="form_register" action="login" method="post">
                    <h2>Connexion</h2>
                    <p class="hint-text">Connecter vous, et commencer à utiliser l'application facilement</p>
                    
                    <div class="form-group">
                        <input id="inp_mail" type="email" class="form-control" name="email" placeholder="Email" required="required">
                    </div>
                    <div class="form-group">
                        <input id="inp_password" type="password" class="form-control" name="password" placeholder="Mot de passe" required="required">
                    </div>
                    <div class="form-group" id="div_btn_success">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Se connecter</button>
                    </div>
                </form>
                <div class="text-center">Vous n'avez pas encore de compte ? <a href="register">S'inscrire</a></div>
            </div>
        </body>
        </div>
        <%
            // -- On pré rempli le champ email --
            if (email_prefilled != null) {
                %>
                    <script>
                        $( document ).ready(function() {
                            $('#inp_mail').val('<%= email_prefilled %>');
                        });
                    </script>
                <%
            }

            // -- On affiche un message de succès --
            if (success_message != null) {
                %>
                    <script>
                        let new_node = $("<div></div>").attr("class","text-center div_p_info");
                        let new_p = $("<p></p>").text('<%= success_message %>').attr("class","p_success");
                        $(new_node).append($(new_p));
                        $("#form_register").append($(new_node));
                    </script>
                <%
            }

            // -- On affiche un message d'erreur --
            if (error_message != null) {
                %> 
                    <script>
                        let new_node = $("<div></div>").attr("class","text-center div_p_info");
                        let new_p = $("<p></p>").text('<%= error_message %>').attr("class","p_error");
                        $(new_node).append($(new_p));
                        $("#form_register").append($(new_node));
                    </script>
                <%
            }
     %>
</html>