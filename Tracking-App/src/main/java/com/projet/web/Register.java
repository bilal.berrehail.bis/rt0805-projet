package com.projet.web;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.User;
import com.projet.token.TokenJwt;
import com.projet.utils.HashUtils;
import com.projet.utils.RegisterUtils;
import com.projet.utils.RequestUtils;
import com.projet.utils.TokenUtils;

@WebServlet(name = "Register", urlPatterns = "/register")
public class Register extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // -- Vérification s'il est déjà connecté, on redirige vers la page d'accueil --
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);
        if (tokenJwt != null) {
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/home");
            return;
        }
        request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Map<String, String[]> parameters = request.getParameterMap();
        // -- Vérifications des champs entrés --
        String result_checked;
        if (!((result_checked = RegisterUtils.checkFieldRegister(parameters)).equals("valid"))) {
            wrongParameters(result_checked, request, response);
            return;
        }

        HibernateController hc = HibernateController.getInstance();
        
        // -- Vérifie si l'adresse email est déjà utilisé --
        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("email", parameters.get("email")[0]);
        List<User> list_user = hc.getEntities(User.class, User.getEntityName(), conditions);
        if (list_user.size() > 0) {
            wrongParameters(RegisterUtils.error_email_used, request, response);
            return;
        }

        // -- Création de l'utilisateur --
        String passwordHash;
        try {
            passwordHash = HashUtils.sha256encrypt(parameters.get("password")[0]);
        } catch (NoSuchAlgorithmException e) {
            wrongParameters("L'algorithme d'hashage est down, et la c'est chaud", request, response);
            return;
        }

        User new_user = new User(parameters.get("last_name")[0], parameters.get("first_name")[0], passwordHash,
                parameters.get("email")[0], Integer.parseInt(parameters.get("age")[0]));
        hc.addEntity(new_user);

        // -- End --
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("email_login_page", new_user.getEmail());
        httpSession.setAttribute("message_login_page", "Votre inscription a bien été prise en compte.");

        response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
    }

    private void wrongParameters(String error_msg, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // -- Pré rempli les champs avec les informations entrées --
        Map<String, String[]> parameter = request.getParameterMap();

        RequestUtils.clearRequest(request);
        request.setAttribute("error", error_msg);
        request.setAttribute("first_name", parameter.get("first_name")[0]);
        request.setAttribute("last_name", parameter.get("last_name")[0]);
        request.setAttribute("age", parameter.get("age")[0]);
        request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
    }
}