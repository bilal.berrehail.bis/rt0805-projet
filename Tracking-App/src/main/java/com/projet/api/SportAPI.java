package com.projet.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Sport;
import com.projet.token.TokenJwt;
import com.projet.utils.TokenUtils;

@WebServlet(name = "SportAPI", urlPatterns = "/api/sport")
public class SportAPI extends HttpServlet {

    public class ResponseSportAPI {
        String message;
        List<Sport> sports;
    }

    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        response.setContentType("application/json; charset=UTF-8");

        ResponseSportAPI responseSportAPI = new ResponseSportAPI();

        if (tokenJwt == null) {
            responseSportAPI.sports = new ArrayList<>();
            responseSportAPI.message = "Token incorrect";
            response.getWriter().println(new Gson().toJson(responseSportAPI));
            return;
        }

        HibernateController hc = HibernateController.getInstance();

        List<Sport> sports = hc.getEntities(Sport.class, Sport.getEntityName(), new HashMap<String, Object>());

        if (sports == null) {
            sports = new ArrayList<>();
        }
        responseSportAPI.sports = sports;
        responseSportAPI.message = "Récupération des sports réussie.";

        response.getWriter().println(new Gson().toJson(responseSportAPI));
    }
}