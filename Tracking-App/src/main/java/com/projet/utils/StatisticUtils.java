package com.projet.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Tracking;

public class StatisticUtils {

    public static Double getDurationOfActivity(Activity activity) {
        List<Tracking> trackings = activity.getTrackings();
        if (trackings == null || trackings.size() == 0 ) {
            return 0.0;
        }

        Date dateStartActivity = activity.getStart_date();
        Date dateEndActivity = trackings.get(trackings.size() - 1).getDate();
        Timestamp tsStartActivity = new Timestamp(dateStartActivity.getTime());
        Timestamp tsEndActivity = new Timestamp(dateEndActivity.getTime());

        Double hourTsStartActivity = tsStartActivity.getTime() / 3600000.0;
        Double hourTsEndActivity = tsEndActivity.getTime() / 3600000.0;

        Double durationOfActivity = hourTsEndActivity - hourTsStartActivity;

        return roundTwoDecimals(durationOfActivity);
    }

    public static Double getDistanceKm(Activity activity) {
        List<Tracking> trackings = activity.getTrackings();
        if (trackings == null || trackings.size() == 0 ) {
            return 0.0;
        }

        List<Tracking> trackingsMock = new ArrayList<>();
        trackingsMock.add(new Tracking(-1, activity.getLatitude(), activity.getLongitude(), null));
        trackingsMock.addAll(trackings);

        Double result = 0.0;
        for (int i = 0; i < trackingsMock.size() - 1; i++) {
            result += getDistanceBetween2Points(trackingsMock.get(i).getLatitude(),
                                                trackingsMock.get(i).getLongitude(),
                                                trackingsMock.get(i + 1).getLatitude(),
                                                trackingsMock.get(i + 1).getLongitude());
        }

        return roundTwoDecimals(result);
    }

    private static Double getDistanceBetween2Points(Double latitude1, Double longitude1, Double latitude2, Double longitude2) {
        return roundTwoDecimals(Math.acos(Math.sin(Math.toRadians(longitude1)) * Math.sin(Math.toRadians(longitude2))
                + Math.cos(Math.toRadians(longitude1)) * Math.cos(Math.toRadians(longitude2))
                        * Math.cos(Math.toRadians(latitude1 - latitude2)))
                * 6371);
    }

    public static Double roundTwoDecimals(Double dbl) {
        return (double) (Math.round(dbl * 100)) / 100;
    }
}