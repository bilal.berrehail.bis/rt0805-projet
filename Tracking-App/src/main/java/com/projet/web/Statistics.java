package com.projet.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.hibernate.HibernateController;
import com.projet.hibernate.model.Activity;
import com.projet.hibernate.model.Sport;
import com.projet.hibernate.model.Tracking;
import com.projet.token.TokenJwt;
import com.projet.utils.RequestUtils;
import com.projet.utils.TokenUtils;

@WebServlet(name = "Statistics", urlPatterns = "/statistics")
public class Statistics extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // -- Vérifications --
        TokenJwt tokenJwt = TokenUtils.verifyToken(request);

        if (tokenJwt == null) {
            response.sendRedirect(RequestUtils.getBaseUrl(request) + "/login");
            return;
        }

        if (request.getParameter("sport_id") == null) {
            response.getWriter().println("Veuillez préciser un sport");
            return;
        }

        // -- Récupération du sport --
        Integer sport_id = Integer.valueOf(request.getParameter("sport_id"));

        HibernateController hc = HibernateController.getInstance();

        HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("id", sport_id);

        List<Sport> sports = hc.getEntities(Sport.class, Sport.getEntityName(), conditions);
        if (sports == null) {
            response.getWriter().println("Le sport choisi n'existe pas");
            return;
        }

        Sport sport = sports.get(0);

        // -- Récupération des activités --
        conditions.clear();
        conditions.put("user_id", tokenJwt.getUser_id());
        conditions.put("sport_id", sport_id);
        conditions.put("is_progress", false);
        conditions.put("is_archive", false);
        
        List<Activity> activities = hc.getEntities(Activity.class, Activity.getEntityName(), conditions);
        if (activities == null) {
            response.getWriter().println("impossible de récuperer les activités");
            return;
        }
        if (activities.size() == 0) {
        	response.getWriter().println("vous n'avez pratiqué aucune activité sur ce sport");
        	return;
        }

        // -- Récupération des trackings --
        for (Activity activity : activities) {
            conditions.clear();
            conditions.put("activity_id", activity.getId());
            List<Tracking> trackings = hc.getEntities(Tracking.class, Tracking.getEntityName(), conditions);
            if (trackings != null && trackings.size() != 0) {
                activity.setTrackings(trackings);
            }
        }

        // -- End --
        request.setAttribute("sport", sport);
        request.setAttribute("activities", activities);
        request.getRequestDispatcher("/jsp/statistics.jsp").forward(request, response);
    }
}