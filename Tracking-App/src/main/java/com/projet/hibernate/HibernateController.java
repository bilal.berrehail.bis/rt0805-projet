package com.projet.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.projet.hibernate.model.HibernateModel;

public class HibernateController {

	private static HibernateController hibernateController;
	private EntityManager entityManager;

	private HibernateController() {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MainConnection");

		this.entityManager = entityManagerFactory.createEntityManager();
	
	}

	public static HibernateController getInstance() {
		if (hibernateController == null) {
			hibernateController = new HibernateController();
		}
		hibernateController.getEntityManager().clear();

		return hibernateController;
	}

	public void beginAndCommit() {
		try {
			entityManager.getTransaction().begin();
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			System.out.println("Une erreur s'est produite (0x02) : " + ex.toString());
		}
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public void addEntity(HibernateModel model) {
		try {
			entityManager.persist(model);
			beginAndCommit();
		} catch (Exception ex) {
			System.out.println("Une erreur s'est produite (0x03) : " + ex.toString());
		}
	}

	public void deleteEntity(HibernateModel model) {
		try {
			entityManager.remove(model);
			beginAndCommit();
		} catch (Exception ex) {
			System.out.println("Une erreur s'est produite (0x04) : " + ex.toString());
		}
	}

	public <T extends HibernateModel> List<T> getEntities(Class<T> hibernateModel, String entity_name,
			HashMap<String, Object> conditions) {
		try {
			StringBuilder query = new StringBuilder();
			query.append(String.format("SELECT t FROM %s t", entity_name));

			if (conditions.size() > 0) {
				query.append(" WHERE ");
			}

			ArrayList<String> keys = new ArrayList<>(conditions.keySet());

			for (int i = 0; i < keys.size(); i++) {
				query.append(String.format("t.%s = :%s", keys.get(i), keys.get(i)));
				if (i != keys.size() - 1) {
					query.append(" and ");
				}
			}

			TypedQuery<T> querySql = entityManager.createQuery(query.toString(), hibernateModel);

			for (int i = 0; i < keys.size(); i++) {
				querySql.setParameter(keys.get(i), conditions.get(keys.get(i)));
			}

			return querySql.getResultList();
		} catch (Exception ex) {
			System.out.println("Une erreur s'est produite (0x05) : " + ex.toString());
			return null;
		}
	}
}